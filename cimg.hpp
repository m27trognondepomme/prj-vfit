#include    <fstream>
#include    <stdint.h>
#include    "cvector.hpp"

#ifndef CIMG_HPP
#define CIMG_HPP

class CImg : public CVector<float> {

public:
    CImg() : CVector(), m_nPlan(0), m_nLigne(0), m_nColonne(0), m_nBitPixel(0), m_hist16(nullptr) {} ;

    ~CImg() { destroy();closeFile();};

    //! \brief destruction de l'image en memoire
    void destroy() ;

    //! \brief creation d'une image en memoire
    void create(
        uint32_t c , //!< nombre de colonne
        uint32_t l , //!< nombre de ligne
        uint8_t  p , //!< nombre de plan ( gray, rvb, ...)
        uint8_t  b   //!< bit par pixel
        ) ;

    //!< \brief indique le nombre de bit par pixel
    void setBitPix(
        int8_t bitpix //!< bit par pixel
        )  ;

    //!< \brief tronque les valeurs min de l'image
    void clipmin(
        float value = 0.f //!< valeur de comparaison pour la troncature
        ) ;
    //!< \brief tronque les valeurs ax de l'image
    void clipmax(
        float value = 1.f //!< valeur de comparaison pour la troncature
        ) ;

    // -------------------------------------------------------------------------
    // Operation sur les fichiers

    //! \brief charge en memoire le fichier image de type ppm ou pgm
    bool readPPM_PGM(
        const char *filename //!< nom du fichier
        ) ;

    //! \brief sauve l'image en memoire dans un fichier image de type pgm ou ppm
    void savePPM_PGM(
        const char *filename, //!< nom du fichier
        bool ascii = false    //! \brief sauve l'image en memoire dans un fichier image de type pgm ou ppm
        ) ;

    //! \brief charge en memoire le fichier image de type pam
    bool readPAM(
        const char *filename //!< nom du fichier
        ) ;
    //! \brief sauve l'image en memoire dans un fichier image de type pam
    void savePAM(
        const char *filename //!< nom du fichier
        ) ;

    //! \brief charge en memoire le fichier image de type fit
    bool readFIT(
        const char *filename //!< nom du fichier
        ) ;

    //! \brief sauve l'image en memoire dans un fichier image de type pam
    void saveFIT(
        const char *filename //!< nom du fichier
        );
    //
    // -------------------------------------------------------------------------

    //! \brief reduction de l'image
    bool crop( uint16_t x,  uint16_t y, uint16_t w , uint16_t h ) ;

    inline float getMaxValue() { return (float) ((1ULL<<abs((int) m_nBitPixel))-1ULL) ; }
    uint16_t getLigne()   { return m_nLigne ;}
    uint16_t getColonne() { return m_nColonne ;}
    uint16_t getPlan()    { return m_nPlan ; }
    uint8_t  getBitPix()  { return abs(m_nBitPixel) ; }
    float    getVisuHaut() { return m_visu_haut ; }
    float    getVisuBas()  { return m_visu_bas ; }
    bool     isFloat() ;

    uint8_t * ArrayRGB8(float seuilBas = 0.f, float seuilHaut = 1.f) ;

    float min( uint8_t nplan=0) ;
    float max( uint8_t nplan=0) ;
    float mean( uint8_t nplan=0) ;
    float stddev( float mean, uint8_t nplan=0) ;
    float median( uint8_t nplan=0) ;

    uint32_t * ArrayHistogram(uint8_t nplan=0,uint32_t bin=256 ) ;

protected:
    void getInfoFileFit( const char *filename ) ;
    void getInfoImgFit_localeC( const char *filename ) ;

    // -------------------------------------------------------------------------
    // Operation sur les fichiers pour chargement des images

    //!< \brief Ouvre et lit l'entete de fichier PPM ou PGM (binaire uniquement)
    bool openFilePPM_PGM(
        const char *filename //!< nom du fichier
        ) ;
    //!< \brief Ouvre et lit l'entete de fichier PAM
    bool openFilePAM(
        const char *filename //!< nom du fichier
        ) ;

    //!< \brief Ouvre et lit l'entete de fichier FIT
    bool openFileFIT(
        const char *filename //!< nom du fichier
        ) ;

    //!< \brief charge l'image en memoire � partir du fichier ouvert
    void loadMemoire() ;

    //!< \brief ferme le fichier ouvert
    void closeFile() ;

    // -------------------------------------------------------------------------
    // Operation sur les fichiers pour sauvegarde des images
    void saveImage8BitsAsciiEnt(std::ofstream &ofs) ;
    void saveImage8BitsEnt(std::ofstream &ofs) ;
    void saveImage16BitsEnt(std::ofstream &ofs) ;
    void saveImage8Bits(std::ofstream &ofs) ;
    void saveImage16Bits(std::ofstream &ofs) ;
    void saveImage32Bits(std::ofstream &ofs) ;
    void saveImageFloat(std::ofstream &ofs) ;

    void setDim(uint16_t ligne,uint16_t colonne, uint8_t nPlan ) ;

    //!< \brief Desentrelace les plans de l'image en memoire
    void desentrelace() ;
    //!< \brief entrelace les plans de l'image en memoire
    void entrelace() ;

    //!< \brief Calcul l'histogramme pour la mediane
    void CalcHist16() ;

    // parametres generiques de  l'image
    uint8_t    m_nPlan    ; //!< nombre de plan ( R,V,B,L,Halpha...)
    uint16_t   m_nLigne   ; //!< nombre de ligne de l'image
    uint16_t   m_nColonne ; //!< nombre de colonne de l'image
    int8_t     m_nBitPixel; //!< nombre de bit par pixel

    // valeur deduite des param�tres d'image
    uint32_t   m_sizePlan  ; //!< taille d'un plan
    uint32_t   m_sizeImage ; //!< taille de l'image
    uint32_t   m_maxvalue  ; //!< valeur max de l'image

    // valeurs specifiques FIT
    float      m_scale     ; //! mise a l'echelle
    float      m_zero      ; //! valeur du zero
    float      m_visu_haut ; //! seuil haut de visualisation (optionnel)
    float      m_visu_bas  ; //! seuil bas  de visualisation (optionnel)

    bool       m_is_plan_entrelace ; //!< flag indiquant si les plans du fichier sont entrelaces

    bool       m_isInMemory ; //!< flag indiquant si l'image est en memoire
    uint32_t   m_pix_pos    ; //!< offset dans le fichier

    std::ifstream  m_ifs    ; //!< descripteur du fichier pour chargement de l'image

    uint32_t   *m_hist16    ; //!< histogramme avec bin de 16bits

};
#endif /* CIMG_HPP */
