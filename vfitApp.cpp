/***************************************************************
 * Name:      vfitApp.cpp
 * Purpose:   Code for Application Class
 * Author:    M27trognondepomme ()
 * Created:   2016-11-05
 * Copyright: M27trognondepomme ()
 * License:
 **************************************************************/

#include    "vfitApp.h"
#include    <wx/filename.h>
#include    <wx/filedlg.h>
#include    <wx/stdpaths.h>
#include    <wx/intl.h>

//(*AppHeaders
#include "vfitMain.h"
#include <wx/image.h>
//*)


IMPLEMENT_APP(vfitApp);

bool vfitApp::OnInit()
{
    bool wxsOK = false ;
    wxString arg1 = wxEmptyString ;
    wxLocale *m_locale;

    if(wxLocale::IsAvailable(wxLANGUAGE_DEFAULT))
    {
        m_locale = new wxLocale(wxLANGUAGE_DEFAULT);

        wxStandardPathsBase &stdPth=wxStandardPaths::Get();
        wxString dataPath= stdPth.GetDataDir() + wxFILE_SEP_PATH + wxT("locale");

        wxLocale::AddCatalogLookupPathPrefix(dataPath);
        m_locale->AddCatalog(_T("vfit"));
    }

    if (wxGetApp().argc == 1)
    {
        wxFileDialog openFileDialog(0, wxT("Display a fit file") ,
                wxEmptyString , wxEmptyString, wxT("fit|*.fit"),
                wxFD_OPEN | wxFD_FILE_MUST_EXIST );

        if (openFileDialog.ShowModal() == wxID_OK)
        {
            arg1 = openFileDialog.GetPath() ;
        }
        else
            return wxsOK ;
    }
    else
    {
        arg1 = wxGetApp().argv[1];
    }

    if ( wxFileName::Exists(arg1 ) )
    {
        wxFileName filename( arg1 ) ;

        if ( (filename.GetExt()).CmpNoCase( wxT("fit")) == 0 )
        {
            wxsOK = true ;
        }
        else
            wxLogError(_("Cannot open file '%s'. (fit?)"), arg1 );
    }
    else
        wxLogError(_("Cannot open file '%s'. (Exist?)"), arg1 );

    if ( wxsOK )
    {
        wxInitAllImageHandlers();
        vfitFrame* Frame = new vfitFrame(0);

        if ( Frame->LoadImage(arg1) )
        {
            Frame->Show();
            Frame->SetTitle(arg1) ;
            SetTopWindow(Frame);
        }
        else
        {
            wxsOK = false ;
            delete Frame ;
        }

    }

    return wxsOK;

}
