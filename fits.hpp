#ifndef FITS_H
#define FITS_H

/*! \brief taille du champ Name d'un mot clef
 */
#define SZ_NKW 8
/*! \brief taille du champ Value Indicator d'un mot clef
 */
#define SZ_IKW 2
/*! \brief taille du champ Value/Comment d'un mot clef
 */
#define SZ_VKW 70
/*! \brief taille d'un mot clef
 */
#define SZ_KW   (SZ_NKW+SZ_IKW+SZ_VKW)

/*! \brief nombre de mot clef par bloc HDU
 */
#define N_KW_HDU   36

/*! \brief taille d'un bloc HDU
 */
#define SZ_HDU   (N_KW_HDU*SZ_KW)

/*! \brief taille maximum d'une chaine du champ Value/Comment d'un mot clef
 */
#define SZ_VKW_STR ( SZ_VKW - 2 )

/*! \brief fin du champs de valeur en mode fixe dans le champ Value/Comment d'un mot clef
 */
#define END_VALFIX_VKW ( 30 - 1 )

/*! \brief fin du champs de valeur en mode fixe dans le HDU
 */
#define END_VALFIX_HDU ( END_VALFIX_VKW  - SZ_NKW - SZ_IKW)
/*
 * *****************************************************************************
 */

/*! \brief HDU Keyword type
 */
typedef enum {
    H_UNDEF   = 0   ,
    H_LOGICAL       ,
    H_INT           ,
    H_FLOAT         ,
    H_INT_CPLX      ,
    H_FLOAT_CPLX    ,
    H_STR
} hdu_keyword_t ;

/*! \brief definition d'un mot clef des Header Data Unit (HDU)
 */
typedef union {
    char buf[SZ_KW]                  ; //!< Buffer contenant un mot clef
    struct {
        char name[SZ_NKW]            ; //!< nom du mot clef
        char value_indicator[SZ_IKW] ; //!< indicateur si une valeur est associée
        char value_comment[SZ_VKW]   ; //!< valeur du mot clef et commentaire
    } field                          ; //!< structure d'un mot clef
} txt_keyword_t  ;

/*! \brief definition d'un bloc de Header Data Unit (HDU)

un HDU est composee de plusieurs blocs HDU dont le dernier doit contenir le mot
clef 'END'.
Apres le 'END', le reste des mots clef non-utilise doit être rempli d'espace.

HDU = HDU_BLOC [ + HDU_BLOC ]*
 */
typedef union {
    char buf[SZ_HDU]                     ; //!< Buffer contenant un bloc HDU
    txt_keyword_t keyword[N_KW_HDU]      ; //!< tableau contenant les mot clef
} txt_hdu_bloc_t  ;


/*!
\brief Bitpix definition
*/
typedef enum bitpix_e {
  p_uint8     =   8 , //!<  Character or unsigned binary integer
  p_uint16    =  16 , //!<  16-bit unsigned binary integer
  p_int16     = -16 , //!<  16-bit two’s complement binary integer
  p_int32     =  32 , //!<  32-bit two’s complement binary integer
  p_int64     =  64 , //!<  64-bit two’s complement binary integer
  p_float     = -32 , //!<  IEEE single precision floating-point
  p_double    = -64   //!<  IEEE double precision floating-point
} bitpix_t ;

typedef union {
    uint8_t  u8 ;
    int16_t  i16;
    uint16_t u16;
    int32_t  i32;
    int64_t  i64;
    float    f;
    double   d;
} pix_t ;

/*!
\brief Information sur une image Fit
*/
typedef struct {
    bitpix_t bitpix   ; //!< nombre de bit par pixel
    uint16_t naxis    ; //!< nombre de plan  (2: mono/ grayscale, 3:RGB,LRGB ... )
    uint32_t naxis1   ; //!< axes 1 lignes
    uint32_t naxis2   ; //!< axes 2 colonnes
    uint32_t naxis3   ; //!< axes 3 ( R , V , B ...)
    double   bzero    ; //!<
    double   bscale   ; //!<
    uint32_t offset   ; //!< offset pour atteindre le debut de l'image
} fits_info_t ;

#endif /* FITS_H */
