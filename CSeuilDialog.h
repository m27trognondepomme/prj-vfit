#ifndef CSEUILDIALOG_H
#define CSEUILDIALOG_H

//(*Headers(CSeuilDialog)
#include <wx/spinctrl.h>
#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/slider.h>
#include <wx/stattext.h>
//*)
#include "WxScrollImage.hpp"
#include "CHistogramDialog.h"

class CSeuilDialog: public wxDialog
{
	public:

		CSeuilDialog(wxWindow* parent,wxWindowID id=wxID_ANY,const wxPoint& pos=wxDefaultPosition,const wxSize& size=wxDefaultSize);
		virtual ~CSeuilDialog();

		//(*Declarations(CSeuilDialog)
		wxSlider* SliderHigh;
		wxSpinCtrl* SpinCtrlHigh;
		wxStaticText* StaticText1;
		wxSpinCtrl* SpinCtrlLow;
		wxSlider* SliderLow;
		wxStaticText* StaticText2;
		//*)

		void setRange( uint32_t vmin, uint32_t vmax ) ;
		void setSeuil( uint32_t seuilBas, uint32_t seuilHaut ) ;
		void setImagePanel(  ScrolledImagePanel *ImagePanel) { m_ImagePanel=ImagePanel;} ;
		void setHistoDialog( CHistogramDialog *HistoDlg) { m_HistoDlg=HistoDlg;} ;
		void RefreshHistogramVisu();
		void RefreshSeuil(bool histo_refresh = false ) ;


	protected:

		//(*Identifiers(CSeuilDialog)
		static const long ID_STATICTEXT1;
		static const long ID_SLIDER1;
		static const long ID_SPINCTRL1;
		static const long ID_STATICTEXT2;
		static const long ID_SLIDER2;
		static const long ID_SPINCTRL2;
		//*)

	private:

		//(*Handlers(CSeuilDialog)
		void OnSliderHighCmdScroll(wxScrollEvent& event);
		void OnSpinCtrlHighChange(wxSpinEvent& event);
		void OnSliderLowCmdScroll(wxScrollEvent& event);
		void OnSpinCtrlLowChange(wxSpinEvent& event);
		//*)

		DECLARE_EVENT_TABLE()

		float getSeuilHaut() ;
		float getSeuilBas() ;

		int  m_seuilHaut ;
		int  m_seuilBas  ;

		ScrolledImagePanel *m_ImagePanel ;
		CHistogramDialog   *m_HistoDlg   ;

};

#endif
