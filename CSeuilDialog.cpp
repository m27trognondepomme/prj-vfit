#include "CSeuilDialog.h"

//(*InternalHeaders(CSeuilDialog)
#include <wx/string.h>
#include <wx/intl.h>
//*)

//(*IdInit(CSeuilDialog)
const long CSeuilDialog::ID_STATICTEXT1 = wxNewId();
const long CSeuilDialog::ID_SLIDER1 = wxNewId();
const long CSeuilDialog::ID_SPINCTRL1 = wxNewId();
const long CSeuilDialog::ID_STATICTEXT2 = wxNewId();
const long CSeuilDialog::ID_SLIDER2 = wxNewId();
const long CSeuilDialog::ID_SPINCTRL2 = wxNewId();
//*)

BEGIN_EVENT_TABLE(CSeuilDialog,wxDialog)
	//(*EventTable(CSeuilDialog)
	//*)
END_EVENT_TABLE()

CSeuilDialog::CSeuilDialog(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size)
{
    m_ImagePanel = nullptr ;

	//(*Initialize(CSeuilDialog)
	wxBoxSizer* BoxSizer3;
	wxBoxSizer* BoxSizer4;
	wxBoxSizer* BoxSizer1;

	Create(parent, id, _("Display threshold"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE|wxCLOSE_BOX, _T("id"));
	SetClientSize(wxDefaultSize);
	Move(wxDefaultPosition);
	BoxSizer1 = new wxBoxSizer(wxVERTICAL);
	StaticText1 = new wxStaticText(this, ID_STATICTEXT1, _("High Threshold"), wxDefaultPosition, wxSize(359,14), 0, _T("ID_STATICTEXT1"));
	BoxSizer1->Add(StaticText1, 0, wxALL|wxEXPAND, 5);
	BoxSizer3 = new wxBoxSizer(wxHORIZONTAL);
	SliderHigh = new wxSlider(this, ID_SLIDER1, 65535, 0, 65535, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_SLIDER1"));
	BoxSizer3->Add(SliderHigh, 1, wxALL|wxEXPAND, 5);
	SpinCtrlHigh = new wxSpinCtrl(this, ID_SPINCTRL1, _T("65535"), wxDefaultPosition, wxSize(73,23), 0, 0, 65535, 65535, _T("ID_SPINCTRL1"));
	SpinCtrlHigh->SetValue(_T("65535"));
	BoxSizer3->Add(SpinCtrlHigh, 0, wxALL|wxEXPAND, 5);
	BoxSizer1->Add(BoxSizer3, 1, wxALL|wxEXPAND, 5);
	StaticText2 = new wxStaticText(this, ID_STATICTEXT2, _("Low threshold"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT2"));
	BoxSizer1->Add(StaticText2, 0, wxALL|wxEXPAND, 5);
	BoxSizer4 = new wxBoxSizer(wxHORIZONTAL);
	SliderLow = new wxSlider(this, ID_SLIDER2, 0, 0, 65535, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_SLIDER2"));
	BoxSizer4->Add(SliderLow, 1, wxALL|wxEXPAND, 5);
	SpinCtrlLow = new wxSpinCtrl(this, ID_SPINCTRL2, _T("0"), wxDefaultPosition, wxSize(72,21), 0, 0, 100, 0, _T("ID_SPINCTRL2"));
	SpinCtrlLow->SetValue(_T("0"));
	BoxSizer4->Add(SpinCtrlLow, 0, wxALL|wxEXPAND, 5);
	BoxSizer1->Add(BoxSizer4, 1, wxALL|wxEXPAND, 5);
	SetSizer(BoxSizer1);
	BoxSizer1->Fit(this);
	BoxSizer1->SetSizeHints(this);

	Connect(ID_SLIDER1,wxEVT_SCROLL_CHANGED,(wxObjectEventFunction)&CSeuilDialog::OnSliderHighCmdScroll);
	Connect(ID_SPINCTRL1,wxEVT_COMMAND_SPINCTRL_UPDATED,(wxObjectEventFunction)&CSeuilDialog::OnSpinCtrlHighChange);
	Connect(ID_SLIDER2,wxEVT_SCROLL_CHANGED,(wxObjectEventFunction)&CSeuilDialog::OnSliderLowCmdScroll);
	Connect(ID_SPINCTRL2,wxEVT_COMMAND_SPINCTRL_UPDATED,(wxObjectEventFunction)&CSeuilDialog::OnSpinCtrlLowChange);
	//*)

	// force le ENTER dans le spinctrl
	SpinCtrlHigh->SetWindowStyle( SpinCtrlHigh->GetWindowStyle() | wxTE_PROCESS_ENTER ) ;
	Connect(ID_SPINCTRL1, wxEVT_COMMAND_TEXT_ENTER,(wxObjectEventFunction)&CSeuilDialog::OnSpinCtrlHighChange);
	SpinCtrlLow->SetWindowStyle( SpinCtrlLow->GetWindowStyle() | wxTE_PROCESS_ENTER ) ;
	Connect(ID_SPINCTRL2, wxEVT_COMMAND_TEXT_ENTER,(wxObjectEventFunction)&CSeuilDialog::OnSpinCtrlLowChange );

	SpinCtrlHigh->SetRange(SliderHigh->GetMin(),SliderHigh->GetMax());
	SpinCtrlLow->SetRange(SliderLow->GetMin(),SliderLow->GetMax()) ;

    m_seuilHaut =  SliderHigh->GetMax();
    m_seuilBas  =  SliderLow->GetMin();
    SpinCtrlHigh->SetValue(m_seuilHaut);
    SpinCtrlLow->SetValue(m_seuilBas);

}

CSeuilDialog::~CSeuilDialog()
{
	//(*Destroy(CSeuilDialog)
	//*)
}

void CSeuilDialog::RefreshSeuil(bool histo_refresh )
{
    if (m_ImagePanel!= nullptr )
    {
        m_ImagePanel->ChangeSeuil( getSeuilBas()  , getSeuilHaut()  );
        if ( histo_refresh )
            RefreshHistogramVisu() ;
    }
}
void CSeuilDialog::setRange( uint32_t vmin, uint32_t vmax )
{
    SliderHigh->SetRange(vmin,vmax);
	SliderLow->SetRange(vmin,vmax) ;
    SpinCtrlHigh->SetRange(vmin,vmax);
	SpinCtrlLow->SetRange(vmin,vmax) ;
}
void CSeuilDialog::setSeuil( uint32_t seuilBas, uint32_t seuilHaut )
{
    if ( seuilBas >= seuilHaut )
        return ;
    m_seuilHaut = seuilHaut ;
    m_seuilBas  = seuilBas  ;

    SliderHigh->SetValue(m_seuilHaut);
    SliderLow->SetValue(m_seuilBas);

    SpinCtrlHigh->SetValue(m_seuilHaut);
    SpinCtrlLow->SetValue(m_seuilBas);

    RefreshSeuil(true);
    Refresh();
}

float CSeuilDialog::getSeuilHaut()
{
    return (float) m_seuilHaut / (float) SliderHigh->GetMax();
}
float CSeuilDialog::getSeuilBas()
{
    return (float) m_seuilBas / (float) SliderLow->GetMax();
}

void CSeuilDialog::OnSliderHighCmdScroll(wxScrollEvent& event)
{
    SpinCtrlHigh->SetValue(SliderHigh->GetValue());
    if (m_ImagePanel!= nullptr )
    {
        m_seuilHaut = SliderHigh->GetValue() ;
        m_seuilBas  = SliderLow->GetValue() ;
        RefreshSeuil(true);
    }
    Refresh();
}

void CSeuilDialog::OnSpinCtrlHighChange(wxSpinEvent& event)
{
    SliderHigh->SetValue(SpinCtrlHigh->GetValue() );
    if (m_ImagePanel!= nullptr )
    {
        m_seuilHaut = SpinCtrlHigh->GetValue() ;
        m_seuilBas  = SpinCtrlLow->GetValue() ;
        RefreshSeuil(true);
    }
    Refresh();
}

void CSeuilDialog::OnSliderLowCmdScroll(wxScrollEvent& event)
{
    SpinCtrlLow->SetValue(SliderLow->GetValue());
    if (m_ImagePanel!= nullptr )
    {
        m_seuilHaut = SliderHigh->GetValue() ;
        m_seuilBas  = SliderLow->GetValue() ;
        RefreshSeuil(true);
    }
    Refresh();
}

void CSeuilDialog::OnSpinCtrlLowChange(wxSpinEvent& event)
{
    SliderLow->SetValue(SpinCtrlLow->GetValue() );
    if (m_ImagePanel!= nullptr )
    {
        m_seuilHaut = SpinCtrlHigh->GetValue() ;
        m_seuilBas  = SpinCtrlLow->GetValue() ;
        RefreshSeuil(true);
    }
    Refresh();
}

void CSeuilDialog::RefreshHistogramVisu()
{
    CImg *img = m_ImagePanel->GetImgFit() ;

    uint32_t bin = 256 ;
    uint32_t *r_hist = nullptr ;
    uint32_t *v_hist = nullptr ;
    uint32_t *b_hist = nullptr ;

    uint32_t *l_hist = nullptr ;
    if ( img->getPlan() == 3 )
    {
        l_hist = m_ImagePanel->ArrayHistogram(0) ;
        r_hist = m_ImagePanel->ArrayHistogram(1) ;
        v_hist = m_ImagePanel->ArrayHistogram(2) ;
        b_hist = m_ImagePanel->ArrayHistogram(3) ;
    }
    else
        l_hist = m_ImagePanel->ArrayHistogram(1) ; // 1 plan suffit

    m_HistoDlg->SetTitle( _("Screen Histogram") );
    m_HistoDlg->SetHistogram( bin, l_hist, r_hist, v_hist, b_hist);

    if (l_hist!= nullptr ) delete [] l_hist ;
    if (r_hist!= nullptr ) delete [] r_hist ;
    if (v_hist!= nullptr ) delete [] v_hist ;
    if (b_hist!= nullptr ) delete [] b_hist ;

    m_HistoDlg->Refresh() ;
}

