/***************************************************************
 * Name:      vfitApp.h
 * Purpose:   Defines Application Class
 * Author:    M27trognondepomme ()
 * Created:   2016-11-05
 * Copyright: M27trognondepomme ()
 * License:
 **************************************************************/

#ifndef VFITAPP_H
#define VFITAPP_H

#include <wx/app.h>

class vfitApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // VFITAPP_H
