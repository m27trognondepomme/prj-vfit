#ifndef WIN32
#include <netinet/in.h>
#include <arpa/inet.h>
#else
#include <winsock2.h>
#include <sys/utime.h>
#endif

#include    <fstream>
#include    <iomanip>
#include    <string>
#include    <clocale>
#include    <cstring>

#include    <cstdlib>
#include    <cmath>

#include    "cimg.hpp"
#include    "fits.hpp"


static uint32_t p2_uint32( uint32_t n )
{
    uint8_t p ;

    for ( p=0;p<32;p++ , n<<=1 )
    {
        if ( n & 0x80000000 )
            break ;
    }

    return 32 - p ;
}

static int check_bitpix( int64_t  bitpix )
{
    int ret ;
    switch ( bitpix )
    {
        case p_uint8  :
        case p_int16  :
        case p_int32  :
        case p_int64  :
        case p_float  :
        case p_double :
            ret= 0 ;
            break;
        default:
            ret= 1 ;
    }

    return ret ;
}

void CImg::setBitPix( int8_t bitpix )
{
    m_nBitPixel = bitpix;
    m_scale     = 1.f ;
    m_zero      = 0.f ;
    if ( bitpix == p_int16 )
        m_zero      = 32768.f ;
    if ( bitpix == p_int32 )
        m_zero      = 2147483648.f ;
    m_visu_bas  =  0.0f ;
    m_visu_haut = -1.0f ;

}

bool CImg::isFloat() { return m_nBitPixel == p_float ;}

void CImg::setDim(uint16_t ligne,uint16_t colonne, uint8_t nPlan )
{
    m_nLigne    = ligne;
    m_nColonne  = colonne;
    m_nPlan     = nPlan;
    m_sizePlan  = ((uint32_t) m_nLigne) * ((uint32_t) m_nColonne) ;
    m_sizeImage = ((uint32_t) m_nPlan) *  m_sizePlan ;
}


void CImg::closeFile()
{
    if ( ! m_ifs.is_open() )
        return ;
    m_ifs.close() ;
    m_pix_pos=0;
}

void CImg::destroy()
{
    CVector::destroy() ;

    setDim( 0,0,0) ;
    setBitPix(0);
    m_isInMemory = false ;
    m_pix_pos    = 0  ;
    m_maxvalue   = 0 ;
    if ( m_hist16!= nullptr )
        delete [] m_hist16 ;
}

void CImg::create( uint32_t c , uint32_t l, uint8_t p , uint8_t b )
{
    destroy() ;

    try
    {
        if ( (c == 0) || (l==0) || (p==0) || ((b<8)&&(b>16)))
            throw("Creation image: dimension incorrecte" ) ;

        setDim( l,c,p) ;
        setBitPix(b);
        m_maxvalue = (1<<b)-1;
        m_isInMemory = true ;
        CVector::create(m_sizeImage);
    }
    catch (const char *err)
    {
        std::cerr << err << std::endl ;
    }

}
// ----------------------------------------------------------------------------
// Chargement des images en memoire
void CImg::loadMemoire()
{
    if ( ! m_ifs.is_open() )
        return ;

    if ( m_sizeImage == 0 )
        return ;

    CVector::create(m_sizeImage);

    m_ifs.seekg( m_pix_pos, m_ifs.beg ) ;

    // lecture du fichier en 1 seule fois
    uint32_t nOctet = abs( (int) m_nBitPixel ) >> 3  ;
    char *memoire = new char[m_sizeImage*nOctet] ;
    m_ifs.read(memoire, m_sizeImage*nOctet );

    float    normalisation = 1.f/  ((float) m_maxvalue ) ;

    switch ( m_nBitPixel )
    {
        case p_uint8 :
            {
                uint8_t *memoire8 = reinterpret_cast<uint8_t *>(memoire);
                // conversion float
                for ( uint32_t ii=0;ii<m_sizeImage;ii++)
                    m_data[ii] = ((float) (*memoire8++) * m_scale + m_zero ) * normalisation  ;
            }
            break ;
        case p_uint16 :
            {
                uint16_t *memoire16 = reinterpret_cast<uint16_t *>(memoire);

                // conversion float avec passage big endian/little endian
                for ( uint32_t ii=0;ii<m_sizeImage;ii++)
                    m_data[ii] = ((float) ntohs(memoire16[ii])* m_scale + m_zero ) * normalisation  ;
            }
            break ;
        case p_int16 :
            {
                uint16_t *memoire16 = reinterpret_cast<uint16_t *>(memoire);

                // conversion float avec passage big endian/little endian
                for ( uint32_t ii=0;ii<m_sizeImage;ii++)
                    m_data[ii] = ((float) ( (int16_t) ntohs(memoire16[ii])) * m_scale + m_zero ) * normalisation  ;
            }
            break ;
        case p_int32 :
            {
                uint32_t *memoire32 = reinterpret_cast<uint32_t *>(memoire);

                // conversion float avec passage big endian/little endian
                for ( uint32_t ii=0;ii<m_sizeImage;ii++)
                   m_data[ii] = ((float) ( (int32_t) ntohl(memoire32[ii]))* m_scale + m_zero  ) * normalisation  ;


            }
            break ;
        case p_float :
            {
                uint32_t *memoire32 = reinterpret_cast<uint32_t *>(memoire);

                // conversion float avec passage big endian/little endian
                for ( uint32_t ii=0;ii<m_sizeImage;ii++)
                {
                    union { float f ; uint32_t i ;} valeur ;
                    valeur.i = ntohl(memoire32[ii]) ;
                    m_data[ii] = valeur.f * m_scale + m_zero  ;
                }
            }
            break ;
        default :
            for ( uint32_t ii=0;ii<m_sizeImage;ii++)
                m_data[ii] = 0.f ;

    }

    delete [] memoire ;
    m_isInMemory = true ;

    if ( m_is_plan_entrelace )
        desentrelace() ;

}

void CImg::desentrelace()
{
    float *tmp = new float[m_sizeImage] ;

    std::memcpy( tmp , m_data , sizeof(float) *m_sizeImage);
    for (uint32_t ii = 0,kk=0; ii < m_sizePlan ; ii++)
    {
        for ( uint32_t offset=0;offset<m_sizeImage;offset+=m_sizePlan ,kk++)
        {
            m_data[ii+offset]  = tmp[kk] ;
        }
    }

    delete [] tmp ;
    m_is_plan_entrelace = false ;
}


void CImg::entrelace()
{
    float *tmp = new float[m_sizeImage] ;

    std::memcpy( tmp , m_data , sizeof(float) *m_sizeImage);
    for (uint32_t ii = 0,kk=0; ii < m_sizePlan ; ii++)
    {
        for ( uint32_t offset=0;offset<m_sizeImage;offset+=m_sizePlan ,kk++)
        {
            m_data[kk]  = tmp[ii+offset] ;
        }
    }

    delete [] tmp ;
    m_is_plan_entrelace = true ;
}


// ----------------------------------------------------------------------------
void CImg::saveImage8BitsAsciiEnt(std::ofstream &ofs)
{
    for (uint32_t ll = 0, ii=0; ll < m_nLigne; ll++)
    {
        for (uint32_t cc = 0; cc < m_nColonne; cc++,ii++)
        {
            for ( uint32_t offset=0;offset<m_sizeImage;offset+=m_sizePlan )
            {
                uint16_t p = std::max(0.f,std::min(1.f, m_data[ii+offset] ) ) * 255;
                ofs << std::setw(3) << p << " "  ;
            }
        }
        ofs << std::endl ;
    }
    ofs.flush();
}

uint8_t * CImg::ArrayRGB8(float seuilBas , float seuilHaut )
{
    uint8_t *buf = nullptr ;

    seuilHaut -= seuilBas ;
    float inv_seuilHaut = 1.f / seuilHaut ;

    if (m_nPlan== 3 )
    {
        buf = new uint8_t[m_sizeImage] ;
        const float max_value = (float) ((1<<8)-1)  ;

        for (uint32_t ii = 0,kk=0; ii < m_sizePlan; ii++)
        {
            for ( uint32_t offset=0;offset<m_sizeImage;offset+=m_sizePlan,kk++ )
            {
                float valeur = (m_data[ii+offset] - seuilBas )* inv_seuilHaut ;
                valeur = std::max(0.f,std::min(1.f, valeur ) ) * max_value ;
                buf[kk] = static_cast<uint8_t>( valeur );
            }
        }
    }
    if (m_nPlan == 1 )
    {
        buf = new uint8_t[3*m_sizeImage] ;
        const float max_value = (float) ((1<<8)-1)  ;

        for (uint32_t ii = 0,kk=0; ii < m_sizePlan; ii++)
        {
            float  valeur = (m_data[ii] - seuilBas )* inv_seuilHaut ;
            valeur = std::max(0.f,std::min(1.f, valeur ) ) * max_value ;
            buf[kk++] = static_cast<uint8_t>( valeur );
            buf[kk++] = static_cast<uint8_t>( valeur );
            buf[kk++] = static_cast<uint8_t>( valeur );
        }
    }

    return buf ;
}

void CImg::saveImage8BitsEnt(std::ofstream &ofs)
{
    uint8_t *buf = new uint8_t[m_sizeImage] ;
    const float max_value = getMaxValue() ;
    const float inv_scale = 1.f / m_scale ;

    for (uint32_t ii = 0,kk=0; ii < m_sizePlan; ii++)
    {
        for ( uint32_t offset=0;offset<m_sizeImage;offset+=m_sizePlan,kk++ )
        {
            float valeur = std::max(0.f,std::min(1.f, m_data[ii+offset] ) ) * max_value ;
            buf[kk] = static_cast<uint8_t>( (valeur-m_zero)*inv_scale );
        }
    }

    ofs.write( (char *)buf, m_sizeImage ) ;
    ofs.flush();
    delete [] buf ;
}


void CImg::saveImage8Bits(std::ofstream &ofs)
{
    uint8_t *buf = new uint8_t[m_sizeImage] ;
    const float max_value = getMaxValue() ;
    const float inv_scale = 1.f / m_scale ;

    for (uint32_t ii = 0; ii < m_sizeImage; ii++)
    {
        float valeur = std::max(0.f,std::min(1.f, m_data[ii] ) ) * max_value ;
        buf[ii] = static_cast<uint8_t>((valeur-m_zero)*inv_scale) ;
    }

    ofs.write( (char *)buf, m_sizeImage ) ;
    ofs.flush();
    delete [] buf ;
}

void CImg::saveImage16BitsEnt(std::ofstream &ofs)
{
    uint16_t *buf = new uint16_t[m_sizeImage] ;
    const float max_value = getMaxValue() ;
    const float inv_scale = 1.f / m_scale ;

    for (uint32_t ii = 0,kk=0; ii < m_sizePlan; ii++)
    {
        for ( uint32_t offset=0;offset<m_sizeImage;offset+=m_sizePlan,kk++ )
        {
            float valeur = std::max(0.f,std::min(1.f, m_data[ii+offset] ) ) * max_value ;
            buf[kk] = htons(static_cast<uint16_t>( (valeur-m_zero )* inv_scale ));
        }
    }

    ofs.write( (char *)buf, m_sizeImage *sizeof(uint16_t) ) ;
    ofs.flush();
    delete [] buf ;
}

void CImg::saveImage16Bits(std::ofstream &ofs)
{
    uint16_t *buf = new uint16_t[m_sizeImage] ;
    const float max_value = getMaxValue() ;
    const float inv_scale = 1.f / m_scale ;

    // std::cout << max_value << " : " << m_zero <<  std::endl ;

    for (uint32_t ii = 0; ii < m_sizeImage; ii++)
    {
        float valeur = std::max(0.f,std::min(1.f, m_data[ii] ) ) * max_value ;
        valeur  = (valeur-m_zero )* inv_scale ;
        buf[ii] = htons(static_cast<int16_t>( valeur ));
    }

    ofs.write( (char *)buf, m_sizeImage *sizeof(uint16_t) ) ;
    ofs.flush();
    delete [] buf ;
}

void CImg::saveImage32Bits(std::ofstream &ofs)
{
    uint32_t *buf = new uint32_t[m_sizeImage] ;
    const float max_value = getMaxValue() ;
    const float inv_scale = 1.f / m_scale ;

    for (uint32_t ii = 0; ii < m_sizeImage ; ii++)
    {
        float valeur = std::max(0.f,std::min(1.f, m_data[ii] ) ) * max_value ;
        buf[ii] = htonl(static_cast<uint32_t>( (valeur-m_zero )* inv_scale ));
    }

    ofs.write( (char *)buf, m_sizeImage *sizeof(uint32_t) ) ;
    ofs.flush();
    delete [] buf ;
}

void CImg::saveImageFloat(std::ofstream &ofs)
{
    uint32_t *buf = new uint32_t[m_sizeImage] ;
    const float max_value = getMaxValue() ;
    const float inv_scale = 1.f / m_scale ;

    for (uint32_t ii = 0; ii < m_sizeImage ; ii++)
    {
        union { float f ; uint32_t i ;} valeur ;

        valeur.f= std::max(0.f,std::min(1.f, m_data[ii] ) ) * max_value ;
        valeur.f = (valeur.f-m_zero )* inv_scale ;
        buf[ii] = htonl( valeur.i);
    }

    ofs.write( (char *)buf, m_sizeImage *sizeof(uint32_t) ) ;
    ofs.flush();
    delete [] buf ;
}

// ------------------- PPM Read File ------------------------------------------

bool CImg::openFilePPM_PGM(const char *filename)
{
    bool ret = false ;
    try
    {
        if ( m_ifs.is_open() )
            m_ifs.close() ;

        // std::ios::binary : need to spec. binary mode for Windows users
        m_ifs.open(filename, std::ios::binary);

        if (m_ifs.fail())
        {
            throw("Ouverture echoue : fichier PPM ");
        }

        std::string header;
        int w, h, m , b , p ;

        m_ifs >> header;
        if (header.compare( "P6") ==  0)
            p = 3 ;
        else {
            if (header.compare( "P5") ==  0)
                p = 1 ;
            else
                throw("Erreur type PPM/PGM: P6 ou P5 attendu ");
        }

        m_ifs >> w >> h >> m;

        b = p2_uint32(m);

        if ( (w == 0) || (h==0) || ((b<8)&&(b>16)))
            throw("Creation image: dimension incorrecte" ) ;

        destroy() ;
        setDim( h,w,p) ;
        setBitPix(b);
        m_maxvalue = m ;

        m_isInMemory = false ;
        m_is_plan_entrelace = true ;

        m_ifs.ignore(256, '\n'); // skip empty lines in necessary until we get to the binary data
        m_pix_pos=m_ifs.tellg() ;

        ret = true ;

    } catch (const char *err)
    {
        std::cerr << err << std::endl ;
        if ( m_ifs.is_open() )
            m_ifs.close() ;
        ret = false ;
    }
    return ret ;
}


bool CImg::readPPM_PGM(const char *filename)
{
    if ( openFilePPM_PGM(filename) )
    {
        loadMemoire() ;
        closeFile() ;
        return true ;
    }
    return false ;
}


// -----------------------------------------------------------------------------
void CImg::savePPM_PGM( const char *filename, bool ascii )
{
    if (m_nColonne == 0 || m_nLigne == 0)
    {
        std::cerr << "Image vide"<< std::endl ;
        return;
    }
    if ((m_nPlan != 3) &&(m_nPlan != 1) )
    {
        std::cerr <<  "Image RGB or Gray uniquement"<< std::endl ;
        return;
    }
    std::ofstream ofs;
    try
    {
        if (ascii)
        {
            ofs.open(filename);

            if (ofs.fail())
                throw("Can't open output file");

            if ( m_nPlan == 1 )
                ofs << "P2 " ;
            else
                ofs << "P3 " ;

            ofs << m_nColonne << " " << m_nLigne <<  " 255\n";
            saveImage8BitsAsciiEnt(ofs);
        }
        else
        {
            ofs.open(filename, std::ios::binary); // need to spec. binary mode for Windows users
            if (ofs.fail())
                throw("Can't open output file");

            if ( m_nPlan == 1 )
                ofs << "P5" ;
            else
                ofs << "P6 " ;

            ofs << std::endl << m_nColonne << std::endl  << m_nLigne << std::endl << "255"<< std::endl ;
            saveImage8BitsEnt( ofs ) ;
        }

        ofs.close();
    }
    catch (const char *err)
    {
        std::cerr << err << std::endl ;
        if ( ofs.is_open() )
            ofs.close();
    }

}
// ------------------- PAM Read & save File ------------------------------------

bool CImg::openFilePAM(const char *filename)
{
    bool ret =false ;
    try
    {
        if ( m_ifs.is_open() )
            m_ifs.close() ;

        // std::ios::binary : need to spec. binary mode for Windows users
        m_ifs.open(filename, std::ios::binary);

        if (m_ifs.fail())
        {
            throw("Ouverture echoue : fichier PAM ");
        }

        std::string header;
        uint32_t w, h, p, b, m;

        m_ifs >> header;
        if (header.compare("P7") != 0)
            throw("Erreur type PAM : P7 attendu ");

        m_ifs >> header;
        if (header.compare("WIDTH") != 0)
            throw("Erreur PAM : WIDTH");
        m_ifs >> header;
        w = std::stoi(header,nullptr,10);

        m_ifs >> header;
        if (header.compare("HEIGHT") != 0)
            throw("Erreur PAM : HEIGHT");
        m_ifs >> header;
        h = std::stoi(header,nullptr,10);

        m_ifs >> header;
        if (header.compare("DEPTH") != 0)
            throw("Erreur PAM : DEPTH");
        m_ifs >> header;
        p = std::stoi(header,nullptr,10);

        m_ifs >> header;
        if (header.compare("MAXVAL") != 0)
            throw("Erreur PAM : MAXVAL");
        m_ifs >> header;
        m = std::stoi(header,nullptr,10);
        b = p2_uint32( m ) ;

        m_ifs >> header;
        if (header.compare("TUPLTYPE") != 0)
            throw("Erreur PAM : TUPLTYPE");
        m_ifs >> header;

        m_ifs >> header;
        if (header.compare("ENDHDR") != 0)
            throw("Erreur PAM : ENDHDR");

        destroy() ;
        setDim( h,w,p) ;
        setBitPix(b);
        m_maxvalue = m ;
        m_isInMemory = false ;
        m_is_plan_entrelace = true ;

        // efface les caractères jusqu'au debut de l'image
        m_ifs.ignore(256, '\n');
        m_pix_pos=m_ifs.tellg() ;
        ret = true ;

    } catch (const char *err)
    {
        std::cerr << err << std::endl ;
        if ( m_ifs.is_open() )
            m_ifs.close() ;
        ret = false ;
    }
    return ret ;
}

bool CImg::readPAM(const char *filename)
{
    if ( openFilePAM(filename) )
    {
        loadMemoire() ;
        closeFile() ;
        return true ;
    }
    return false ;
}
// -----------------------------------------------------------------------------

void CImg::savePAM( const char *filename )
{
    static const char tupltype[4][16] = { "GRAYSCALE","GRAYSCALE_ALPHA","RGB","RGB_ALPHA" } ;
    if (m_nColonne == 0 || m_nLigne == 0)
    {
        std::cerr << "Image vide"<< std::endl ;
        return;
    }
    if ( (m_nPlan < 1) && (m_nPlan > 4) )
    {
        std::cerr << "Image Monochrome,RGB, RGB+ALPHA uniquement"<< std::endl ;
        return;
    }
    if ( (m_nBitPixel != p_uint8 ) && (m_nBitPixel != p_uint16 ) )
    {
        std::cerr << "Image 8/16bits uniquement"<< std::endl ;
        return;
    }
    std::ofstream ofs;
    try
    {
        ofs.open(filename, std::ios::binary); // need to spec. binary mode for Windows users
        if (ofs.fail())
            throw("Can't open output file");

        uint32_t max_value = getMaxValue() ;

        ofs << "P7\nWIDTH "   << m_nColonne
            << "\nHEIGHT "    << m_nLigne
            << "\nDEPTH "     << (int16_t) m_nPlan
            << "\nMAXVAL "    << max_value
            << "\nTUPLTYPE "  << tupltype[m_nPlan-1]
            << "\nENDHDR\n"
            ;

        if ( m_nBitPixel == p_uint8 )
        {
            saveImage8BitsEnt( ofs ) ;
        }
        else
        {
            saveImage16BitsEnt( ofs ) ;
        }

        ofs.close();
    }
    catch (const char *err)
    {
        std::cerr << err << std::endl ;
        ofs.close();
    }

}

// -----------------------------------------------------------------------------

bool CImg::openFileFIT( const char *filename )
{
    bool ret = false ;
    std::string old_locale, saved_locale;

    old_locale = std::setlocale (LC_ALL, NULL);

    saved_locale = old_locale;

    std::setlocale (LC_ALL, "C" );

    try
    {
        if ( m_ifs.is_open() )
            m_ifs.close() ;

        // std::ios::binary : need to spec. binary mode for Windows users
        m_ifs.open(filename, std::ios::binary);

        if (m_ifs.fail())
            throw("Ouverture echoue : fichier fits ");

        txt_hdu_bloc_t   thdu_bloc  ;
        int64_t          entier_lu  ;
        double           float_lu   ;
        char            *endptr     ;
        uint8_t          ii         ;
        int              cptbloc = 0;
        bool             bContinue= true ;

        bitpix_t bitpix = p_int16 ;
        uint16_t naxis  = 0   ;
        uint16_t naxis1 = 1   ;
        uint16_t naxis2 = 1   ;
        uint16_t naxis3 = 1   ;
        float    bzero  = 0.f ;
        float    bscale = 1.f ;
        float    visu_haut = -1.0f ;
        float    visu_bas  =  0.0f ;

        memset(thdu_bloc.buf,0,SZ_HDU);

        m_ifs.read(thdu_bloc.buf, sizeof(txt_hdu_bloc_t) ) ;
        if (m_ifs.fail())
            throw("Read HDU echoue : fichier fits ");

        // =========================================================================
        //  verification des mots cles et des valeurs associees

        // -------------------------------------------------------------------------
        // SIMPLE
        if ( strncmp(thdu_bloc.keyword[0].field.name , "SIMPLE  ",SZ_NKW) != 0 )
            throw("Invalid first keyword (SIMPLE)\n");

        // -------------------------------------------------------------------------
        // BITPIX
        if ( strncmp(thdu_bloc.keyword[1].field.name , "BITPIX  ",SZ_NKW) != 0 )
            throw("Invalid 2nd keyword (BITPIX)\n");

        entier_lu = strtoll(thdu_bloc.keyword[1].field.value_comment,&endptr, 0 ) ;

        if ( endptr == thdu_bloc.keyword[1].field.value_comment )
            throw("Invalid BITPIX value \n");

        entier_lu = entier_lu == p_uint16 ? p_int16 : entier_lu ;

        if ( check_bitpix(entier_lu) )
            throw("Invalid BITPIX value x\n");
        bitpix = (bitpix_t) entier_lu ;

        // -------------------------------------------------------------------------
        // NAXIS
        if ( strncmp(thdu_bloc.keyword[2].field.name , "NAXIS   ",SZ_NKW) != 0 )
            throw("Invalid 3ie keyword (NAXIS)\n");
        entier_lu = strtoll(thdu_bloc.keyword[2].field.value_comment,&endptr, 0 ) ;

        if ( endptr == thdu_bloc.keyword[2].field.value_comment )
            throw("Invalid NAXIS value \n");

        //if ( (entier_lu != 2 ) && (entier_lu != 3 ) )
        //    throw("Unsupported NAXIS value : only 2 or 3\n");
        naxis = entier_lu ;

        // -------------------------------------------------------------------------
        // NAXIS1
        if ( strncmp(thdu_bloc.keyword[3].field.name , "NAXIS1  ",SZ_NKW) != 0 )
            throw("Invalid 4ie keyword (NAXIS1)\n");
        entier_lu = strtoll(thdu_bloc.keyword[3].field.value_comment,&endptr, 0 ) ;

        if ( endptr == thdu_bloc.keyword[3].field.value_comment )
            throw("Invalid NAXIS1 value \n");
        if ( (entier_lu <= 0  ) )
            throw("Invalid NAXIS1 value \n");
        naxis1 = entier_lu ;

        // -------------------------------------------------------------------------
        // NAXIS2
        if ( strncmp(thdu_bloc.keyword[4].field.name , "NAXIS2  ",SZ_NKW) != 0 )
            throw("Invalid 5ie keyword (NAXIS1)\n");
        entier_lu = strtoll(thdu_bloc.keyword[4].field.value_comment,&endptr, 0 ) ;

        if ( endptr == thdu_bloc.keyword[4].field.value_comment )
            throw("Invalid NAXIS2 value \n");
        if ( (entier_lu <= 0  ) )
            throw("Invalid NAXIS2 value \n");
        naxis2 = entier_lu ;

        // -------------------------------------------------------------------------
        // NAXIS3
        ii=5;
        if ( naxis > 2 )
        {
            if ( strncmp(thdu_bloc.keyword[ii].field.name , "NAXIS3  ",SZ_NKW) != 0 )
                throw("Invalid 6ie keyword (NAXIS1)\n");
            entier_lu = strtoll(thdu_bloc.keyword[ii].field.value_comment,&endptr, 0 ) ;

            if ( endptr == thdu_bloc.keyword[ii].field.value_comment )
                throw("Invalid NAXIS3 value \n");
            if ( (entier_lu <= 0  ) )
                throw("Invalid NAXIS3 value \n");
            naxis3 = entier_lu ;
            ii++;
        }

        while ( bContinue )
        {
            // -------------------------------------------------------------------------
            // BZERO , BSCALE
            for ( ;ii< N_KW_HDU ;ii++)
            {
                if ( strncmp(thdu_bloc.keyword[ii].field.name , "MIPS-HI ",SZ_NKW) == 0 )
                {
                    float_lu = strtod(thdu_bloc.keyword[ii].field.value_comment,&endptr) ;
                    if ( endptr == thdu_bloc.keyword[ii].field.value_comment )
                        throw("Invalid MIPS-HI value \n");
                    visu_haut = (float) float_lu ;
                }
                if ( strncmp(thdu_bloc.keyword[ii].field.name , "MIPS-LO ",SZ_NKW) == 0 )
                {
                    float_lu = strtod(thdu_bloc.keyword[ii].field.value_comment,&endptr) ;
                    if ( endptr == thdu_bloc.keyword[ii].field.value_comment )
                        throw("Invalid MIPS-LO value \n");
                    visu_bas = (float) float_lu ;
                }

                if ( strncmp(thdu_bloc.keyword[ii].field.name , "BZERO   ",SZ_NKW) == 0 )
                {
                    float_lu = strtod(thdu_bloc.keyword[ii].field.value_comment,&endptr) ;
                    if ( endptr == thdu_bloc.keyword[ii].field.value_comment )
                        throw("Invalid BZERO value \n");
                    bzero = (float) float_lu ;
                }

                if ( strncmp(thdu_bloc.keyword[ii].field.name , "BSCALE  ",SZ_NKW) == 0 )
                {
                    float_lu = strtod(thdu_bloc.keyword[ii].field.value_comment,&endptr) ;
                    if ( endptr == thdu_bloc.keyword[ii].field.value_comment )
                        throw("Invalid BSCALE value \n");
                    bscale = (float) float_lu ;
                }

                if (strncmp(thdu_bloc.keyword[ii].field.name , "END     ",SZ_NKW) == 0)
                {
                    bContinue = false ;
                    break ;
                }

                //print_field(thdu_bloc.keyword[ii].buf, SZ_KW , '\n' ) ;

            }
            cptbloc++ ;

            if (bContinue != false )
            {
                memset(thdu_bloc.buf,0,SZ_HDU);

                m_ifs.read(thdu_bloc.buf, sizeof(txt_hdu_bloc_t) ) ;
                if (m_ifs.fail())
                    throw("Read HDU echoue : fichier fits ");

                ii=0 ;
            }
        }

        destroy() ;
        setDim( naxis2,naxis1,naxis3 ) ;
        setBitPix(bitpix);
        m_scale             = bscale ;
        m_zero              = bzero  ;
        m_maxvalue = (uint32_t)((1LL<<abs((int) bitpix))-1);
        if ( visu_haut > visu_bas )
        {
            float    normalisation = 1.f/  ((float) m_maxvalue ) ;
            m_visu_haut = (visu_haut * m_scale + m_zero ) * normalisation  ;
            m_visu_bas  = (visu_bas  * m_scale + m_zero ) * normalisation  ;
        }

        m_isInMemory        = false ;
        m_is_plan_entrelace = false ;
        m_pix_pos           = cptbloc * SZ_HDU  ;

        ret = true ;

    } catch (const char *err)
    {
        std::cerr << err << std::endl ;
        if ( m_ifs.is_open() )
            m_ifs.close() ;

        ret = false ;
    }

    std::setlocale (LC_ALL,saved_locale.c_str() );

    return ret ;
}


bool CImg::readFIT(const char *filename)
{
    if ( openFileFIT(filename) )
    {
        loadMemoire() ;
        closeFile() ;

        return true ;
    }

    return false ;
}


void CImg::saveFIT( const char *filename )
{
    if (m_nColonne == 0 || m_nLigne == 0)
    {
        std::cerr << "Image vide"<< std::endl ;
        return;
    }
    if ( (m_nPlan < 1) && (m_nPlan > 4) )
    {
        std::cerr << "Image Monochrome,RGB, RGB+ALPHA uniquement"<< std::endl ;
        return;
    }

    std::ofstream ofs;
    try
    {
        ofs.open(filename , std::ios::binary );
        if (ofs.fail())
            throw("Can't open output file");

        uint16_t naxis = m_nPlan == 1 ? 2 : m_nPlan ;
        uint16_t naxis1 = m_nColonne ;
        uint16_t naxis2 = m_nLigne   ;
        uint16_t naxis3 = m_nPlan == 1 ? 0 : m_nPlan ;
        uint16_t bitpix = (int16_t) abs(m_nBitPixel) ;

        // << setprecision(0),scientific,fixed, dec,oct,hex
        ofs << "SIMPLE  = " << std::setw(20)               << "T"     << std::setw(50) << std::left << " / file does conform to FITS standard" ;
        ofs << "BITPIX  = " << std::setw(20) << std::right << bitpix  << std::setw(50) << std::left << " / number of bits per data pixel"      ;
        ofs << "NAXIS   = " << std::setw(20) << std::right << naxis   << std::setw(50) << std::left << " / number of bits per data pixel"      ;
        ofs << "NAXIS1  = " << std::setw(20) << std::right << naxis1  << std::setw(50) << std::left << " / width ( data pixel) of image"       ;
        ofs << "NAXIS2  = " << std::setw(20) << std::right << naxis2  << std::setw(50) << std::left << " / height ( data pixel) of image"      ;
        if ( naxis > 2 )
            ofs << "NAXIS3  = " << std::setw(20) << std::right << naxis3  << std::setw(50) << std::left << " / number of plan of image";
        ofs << "BSCALE  = " << std::setw(20) << std::right << std::scientific << m_scale << std::setw(50) << std::left << " / Real = TAPE*BSCALE + BZERO";
        ofs << "BZERO   = " << std::setw(20) << std::right << std::scientific << m_zero  << std::setw(50) << std::left << " / Real = TAPE*BSCALE + BZERO";
        ofs << "END" << std::setw(77) << " ";

        ofs << std::setw((N_KW_HDU-(8+( naxis > 2 )))*SZ_KW)  << " " ;

        switch ( m_nBitPixel )
        {
            case p_uint8 :
                    saveImage8Bits( ofs ) ;
                    ofs << std::setfill('\0') << std::setw(SZ_HDU - m_sizeImage%SZ_HDU)  << '\0' ;
                    //std::cout << "reste HDU 8bits:" << SZ_HDU - m_sizeImage%SZ_HDU  << std::endl ;
                break ;
            case p_int16 :
            case p_uint16 :
                    saveImage16Bits( ofs ) ;
                    ofs << std::setfill('\0')  << std::setw(SZ_HDU - (m_sizeImage*2)%SZ_HDU)  << '\0' ;
                    //std::cout << "reste HDU 16bits:" << SZ_HDU - (m_sizeImage*2)%SZ_HDU  << std::endl ;
                break ;
            case p_int32 :
                    saveImage32Bits( ofs ) ;
                    ofs << std::setfill('\0')  << std::setw(SZ_HDU - (m_sizeImage*4)%SZ_HDU)  << '\0' ;
                    //std::cout << "reste HDU 32bits:" << SZ_HDU - (m_sizeImage*4)%SZ_HDU  << std::endl ;
                break ;
            case p_float :
                    saveImageFloat( ofs ) ;
                    ofs << std::setfill('\0')  << std::setw(SZ_HDU - (m_sizeImage*4)%SZ_HDU)  << '\0' ;
                    //std::cout << "reste HDU float:" << SZ_HDU - (m_sizeImage*4)%SZ_HDU  << std::endl ;
                break ;
            default : ;
        }



        ofs.close();
    }
    catch (const char *err)
    {
        std::cerr << err << std::endl ;
        ofs.close();
    }

}

// -----------------------------------------------------------------------------

void CImg::clipmin( float value )
{
    for (uint32_t ii = 0; ii < m_items ; ii++)
    {
        m_data[ii] = std::max(value, m_data[ii] ) ;
    }
}

void CImg::clipmax( float value )
{
    for (uint32_t ii = 0; ii < m_items ; ii++)
    {
        m_data[ii]=std::min(value, m_data[ii] ) ;
    }
}

float CImg::max( uint8_t nplan)
{
    if (nplan >  m_nPlan)
        return 1.0 ;
    uint32_t offset  = (nplan == 0) ?  0      : m_sizePlan*(nplan-1) ;
    uint32_t nb_item = (nplan == 0) ? m_items : m_sizePlan       ;
    float v_max = m_data[offset++];
    for (uint32_t ii = 1; ii < nb_item ; ii++,offset++)
    {
        v_max = (v_max>=m_data[offset] )? v_max:m_data[offset]  ;
    }

    return v_max ;
}

float CImg::min( uint8_t nplan)
{
    if (nplan > m_nPlan)
        return -1.0 ;

    uint32_t offset  = (nplan == 0) ?  0      : m_sizePlan*(nplan-1) ;
    uint32_t nb_item = (nplan == 0) ? m_items : m_sizePlan       ;
    float v_min = m_data[offset++];
    for (uint32_t ii = 1; ii < nb_item ; ii++,offset++)
    {
        v_min=(v_min<=m_data[offset] )? v_min:m_data[offset]  ;
    }

    return v_min ;
}

float CImg::mean( uint8_t nplan)
{
    if (nplan >  m_nPlan)
        return 0.0 ;
    uint32_t offset  = (nplan == 0) ?  0      : m_sizePlan*(nplan-1) ;
    uint32_t nb_item = (nplan == 0) ? m_items : m_sizePlan       ;
    double v_sum = 0 ;
    double scale = 1.f/ (double) nb_item ;
    for (uint32_t ii = 0; ii < nb_item ; ii++,offset++)
    {
        v_sum += m_data[offset]  ;
    }

    return v_sum*scale ;
}

float CImg::stddev( float mean, uint8_t nplan)
{
    if (nplan >  m_nPlan)
        return 0.0 ;

    uint32_t offset  = (nplan == 0) ?  0      : m_sizePlan*(nplan-1) ;
    uint32_t nb_item = (nplan == 0) ? m_items : m_sizePlan       ;

    double v_sum  = 0 ;
    double v_mean = mean ;

    for (uint32_t ii = 0; ii < nb_item ; ii++,offset++)
    {
        double x =  v_mean - m_data[offset] ;
        v_sum +=  ( x* x );
    }

    return sqrt( v_sum / (double) (nb_item) )  ;
}

void CImg::CalcHist16()
{
    if ( m_hist16 != nullptr )
        return ;

    const uint32_t  bin = 1<<16;
    m_hist16 = new uint32_t [bin*m_nPlan] ;

    for ( uint32_t ii=0;ii<bin*m_nPlan;ii++)
        m_hist16[ii] = 0 ;

    //float seuilBas  = min(0)  ;
    //seuilBas = seuilBas < 0 ? seuilBas : 0 ;

    float  fact     =(float)bin-1.f;
    uint32_t offset =0 ;
    for ( uint32_t jj=0;jj<m_nPlan;jj++)
    {
        uint32_t kk = jj*bin ;
        for (uint32_t ii = 0; ii < m_sizePlan ; ii++,offset++)
        {
            float  valeur = m_data[offset] ; // - seuilBas ;
            int32_t index = (valeur<0) ? 0 : (valeur> 1.0 )? fact : (uint32_t) round(fact*valeur);
            m_hist16[kk+index]++ ;
        }
    }

}

float CImg::median( uint8_t nplan)
{
    if (nplan >  m_nPlan)
        return 0.0 ;

    const uint32_t  bin = 1<<16;
    CalcHist16();

    if ( (nplan == 0)&&(m_nPlan==1))
        nplan = 1 ;

    uint32_t ii    ;
    uint32_t cnt   ;
    uint32_t pos0  ;
    uint32_t pos1  ;
    uint32_t ii1=0 ;
    uint32_t *bHistogram ;
    float    m ;
    uint32_t nb_plan ;

    if ( (nplan == 0)&&(m_nPlan>1))
    {
        bHistogram = &m_hist16[0];
        nb_plan    = m_nPlan     ;
    }
    else
    {
        bHistogram = &m_hist16[bin*(nplan-1)];
        nb_plan    = 1 ;
    }
    pos0 = nb_plan*m_sizePlan/2;
    pos1 = m_sizePlan&1 ? pos0 : pos0-1 ;
    for (ii=0,cnt=0;ii<bin;ii++)
    {
        for ( uint32_t jj=0;jj<nb_plan;jj++)
            cnt += bHistogram[jj*bin+ii] ;
        if ( cnt >=  pos1 + 1)
            ii1=ii ;
        if ( cnt >=  pos0 + 1)
            break ;
    }
    m= ((float) ii1 + (float) ii ) /2.f ;
    return m/bin ;
}

uint32_t * CImg::ArrayHistogram( uint8_t nplan, uint32_t bin )
{
    if (nplan >  m_nPlan)
        return nullptr ;

    uint32_t  nn = 3 ;
    uint32_t  nItems = ( nplan == 0 )? m_items : m_sizePlan;

    uint32_t *bHistogram = new uint32_t [bin+nn] ;

    for ( uint32_t ii=0;ii<bin+nn;ii++)
        bHistogram[ii] = 0 ;

    uint32_t  offset = ( nplan == 0 )? 0 : m_sizePlan*(nplan-1) ;
    float     fact   =(float)bin-1.f;
    for (uint32_t ii = 0; ii < nItems ; ii++,offset++)
    {
        int32_t index = (m_data[offset]<0) ?
                                0 : (m_data[offset]> 1.0 )?
                                        fact : (uint32_t) round(fact*m_data[offset]);
        bHistogram[index]++ ;
    }

    for ( uint32_t ii = 1; ii < bin ; ii++)
    {
        if ( bHistogram[ii] != 0 )
        {
            bHistogram[bin] = ii ;
            break ;
        }
    }
    for ( uint32_t ii = 1; ii < bin ; ii++)
    {
        if ( bHistogram[bin-1-ii] != 0 )
        {
            bHistogram[bin+1] = bin-1-ii ;
            break ;
        }
    }

    uint32_t n_pts=0 ;
    for ( uint32_t ii = 1; ii < bin-1 ; ii++)
    {
        n_pts+=bHistogram[ii] ;
    }

    uint32_t sum=0 ;
    for ( uint32_t ii = 1; ii < bin-1 ; ii++)
    {
        sum+=bHistogram[ii] ;
        if ( sum>= n_pts/2 )
        {
            bHistogram[bin+2] = ii ;
            break ;
        }
    }

    return bHistogram ;
}

// -----------------------------------------------------------------------------
bool CImg::crop( uint16_t x0,  uint16_t y0, uint16_t w , uint16_t h )
{
    uint16_t col  = getColonne();
    uint16_t lig  = getLigne();
    uint16_t plan = getPlan() ;

    if ( (x0 >=  col) || ((x0+w) >  col) ||  (y0 >=  lig) || ((y0+h) >  lig) )
        return true ;

    float *img_crop = new float [col*lig*plan] ;

    if ( img_crop == NULL )
        return true ;

    uint32_t ii = 0 ;
    uint32_t kk = 0 ;
    for ( uint32_t offset=0;offset<m_sizeImage;offset+=m_sizePlan )
    {
        kk=offset+y0*col;
        //std::cout << "::" << kk <<std::endl ;
        for (uint32_t ll = y0; ll < y0+h ; ll++)
        {
            for (uint32_t cc = x0; cc < x0+w ; cc++,ii++)
            {
                img_crop[ii]= m_data[kk+cc] ;
            }
            kk+=col;
        }
    }

    memcpy( m_data,img_crop,ii*sizeof(float));
    setDim(h,w,plan );

    delete [] img_crop ;

    return false ;
}

