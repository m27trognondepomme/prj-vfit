/***************************************************************
 * Name:      vfitMain.cpp
 * Purpose:   Code for Application Frame
 * Author:    M27trognondepomme ()
 * Created:   2016-11-05
 * Copyright: M27trognondepomme ()
 * License:
 **************************************************************/
#include    <wx/msgdlg.h>
#include    <wx/aboutdlg.h>
#include    <wx/filename.h>
#include    <wx/dir.h>
#include    <wx/numformatter.h>
#include    <wx/stdpaths.h>
#include    <wx/platinfo.h>

#include    "fits.hpp"
#include    "vfitMain.h"

//(*InternalHeaders(vfitFrame)
#include <wx/string.h>
#include <wx/intl.h>
#include <wx/bitmap.h>
#include <wx/image.h>
//*)


//(*IdInit(vfitFrame)
const long vfitFrame::ID_OPEN_FIT = wxNewId();
const long vfitFrame::idMenuQuit = wxNewId();
const long vfitFrame::ID_NEXT = wxNewId();
const long vfitFrame::ID_PREC = wxNewId();
const long vfitFrame::ID_FIRST = wxNewId();
const long vfitFrame::ID_LAST = wxNewId();
const long vfitFrame::ID_MENUITEM8 = wxNewId();
const long vfitFrame::ID_MENUITEM15 = wxNewId();
const long vfitFrame::ID_WB = wxNewId();
const long vfitFrame::ID_MENUITEM10 = wxNewId();
const long vfitFrame::ID_MENUITEM11 = wxNewId();
const long vfitFrame::ID_MENUITEM12 = wxNewId();
const long vfitFrame::ID_MENUITEM13 = wxNewId();
const long vfitFrame::ID_MENUITEM7 = wxNewId();
const long vfitFrame::ID_MENUITEM9 = wxNewId();
const long vfitFrame::ID_MENUITEM5 = wxNewId();
const long vfitFrame::ID_MENUITEM6 = wxNewId();
const long vfitFrame::ID_MENUITEM1 = wxNewId();
const long vfitFrame::ID_MENUITEM2 = wxNewId();
const long vfitFrame::ID_MENUITEM3 = wxNewId();
const long vfitFrame::ID_MENUITEM4 = wxNewId();
const long vfitFrame::ID_MENUITEM14 = wxNewId();
const long vfitFrame::idMenuAbout = wxNewId();
const long vfitFrame::IDB_QUIT = wxNewId();
const long vfitFrame::IDB_OPEN = wxNewId();
const long vfitFrame::IDB_HOME = wxNewId();
const long vfitFrame::IDB_PREVIOUS = wxNewId();
const long vfitFrame::IDB_NEXT = wxNewId();
const long vfitFrame::IDB_END = wxNewId();
const long vfitFrame::IDB_PZOOM = wxNewId();
const long vfitFrame::IDB_MZOOM = wxNewId();
const long vfitFrame::IDB_FIT = wxNewId();
const long vfitFrame::IDB_NOZOOM = wxNewId();
const long vfitFrame::ID_TOOLBARITEM4 = wxNewId();
const long vfitFrame::IDB_SEUIL = wxNewId();
const long vfitFrame::IDB_WB = wxNewId();
const long vfitFrame::IDB_HISTOIMG = wxNewId();
const long vfitFrame::IDB_HISTOECRAN = wxNewId();
const long vfitFrame::IDB_HISTOPREF = wxNewId();
const long vfitFrame::ID_TOOLBARITEM1 = wxNewId();
const long vfitFrame::IDB_STAT_IMAGE = wxNewId();
const long vfitFrame::IDB_ROTATELEFT = wxNewId();
const long vfitFrame::IDB_ROTATERIGHT = wxNewId();
const long vfitFrame::IDB_FLIPH = wxNewId();
const long vfitFrame::IDB_FLIPV = wxNewId();
const long vfitFrame::IDB_MENU = wxNewId();
const long vfitFrame::ID_TOOLBARITEM3 = wxNewId();
const long vfitFrame::ID_TOOLBAR1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(vfitFrame,wxFrame)
    //(*EventTable(vfitFrame)
    //*)
END_EVENT_TABLE()

vfitFrame::vfitFrame(wxWindow* parent,wxWindowID id)
{
    wxStandardPathsBase &stdPth=wxStandardPaths::Get();
    wxString dataPath=stdPth.GetDataDir();

    wxString cwd = wxGetCwd( ) ;
    wxSetWorkingDirectory( dataPath );

    //(*Initialize(vfitFrame)
    wxMenuItem* MenuItem2;
    wxMenuItem* MenuItem1;
    wxMenu* Menu1;
    wxMenuBar* MenuBar1;
    wxMenu* Menu2;

    Create(parent, id, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, _T("id"));
    SetClientSize(wxSize(640,480));
    SetMinSize(wxSize(320,240));
    MenuBar1 = new wxMenuBar();
    Menu1 = new wxMenu();
    MenuItem18 = new wxMenuItem(Menu1, ID_OPEN_FIT, _("&Open"), _("Open a FIT image"), wxITEM_NORMAL);
    Menu1->Append(MenuItem18);
    MenuItem1 = new wxMenuItem(Menu1, idMenuQuit, _("&Quit\tAlt-F4"), _("Quit the application"), wxITEM_NORMAL);
    Menu1->Append(MenuItem1);
    MenuBar1->Append(Menu1, _("&File"));
    Menu7 = new wxMenu();
    MenuItem19 = new wxMenuItem(Menu7, ID_NEXT, _("&Next image"), _("Open the next image"), wxITEM_NORMAL);
    Menu7->Append(MenuItem19);
    MenuItem20 = new wxMenuItem(Menu7, ID_PREC, _("&Previous image "), _("Open the previous image"), wxITEM_NORMAL);
    Menu7->Append(MenuItem20);
    MenuItem21 = new wxMenuItem(Menu7, ID_FIRST, _("&First image"), _("Open the first image"), wxITEM_NORMAL);
    Menu7->Append(MenuItem21);
    MenuItem22 = new wxMenuItem(Menu7, ID_LAST, _("&Last image"), _("Open the last image"), wxITEM_NORMAL);
    Menu7->Append(MenuItem22);
    MenuBar1->Append(Menu7, _("&Navigation"));
    Menu5 = new wxMenu();
    MenuItem10 = new wxMenuItem(Menu5, ID_MENUITEM8, _("Threshold"), _("Manual tuning of display thresholds"), wxITEM_NORMAL);
    Menu5->Append(MenuItem10);
    MenuItem17 = new wxMenuItem(Menu5, ID_MENUITEM15, _("Auto threshold"), _("Automatic tuning of display thresholds"), wxITEM_NORMAL);
    Menu5->Append(MenuItem17);
    MenuItem23 = new wxMenuItem(Menu5, ID_WB, _("White balance"), wxEmptyString, wxITEM_NORMAL);
    Menu5->Append(MenuItem23);
    MenuItem12 = new wxMenuItem(Menu5, ID_MENUITEM10, _("Statistic"), _("Statistic of FIT image"), wxITEM_NORMAL);
    Menu5->Append(MenuItem12);
    MenuItem13 = new wxMenuItem(Menu5, ID_MENUITEM11, _("Histogram of FIT file"), _("Display the Histogram of FIT file"), wxITEM_NORMAL);
    Menu5->Append(MenuItem13);
    MenuItem14 = new wxMenuItem(Menu5, ID_MENUITEM12, _("Histogram of the screen"), _("Display the Histogram of screen"), wxITEM_NORMAL);
    Menu5->Append(MenuItem14);
    MenuItem15 = new wxMenuItem(Menu5, ID_MENUITEM13, _("Histogram linear/Logarithmic"), _("switch linear or logarithmic display"), wxITEM_NORMAL);
    Menu5->Append(MenuItem15);
    MenuBar1->Append(Menu5, _("&Visualization"));
    Menu4 = new wxMenu();
    MenuItem9 = new wxMenuItem(Menu4, ID_MENUITEM7, _("Rotate +90 deg."), _("Right rotate of the image"), wxITEM_NORMAL);
    Menu4->Append(MenuItem9);
    MenuItem11 = new wxMenuItem(Menu4, ID_MENUITEM9, _("Rotate -90 deg."), _("Right  rotate of the image"), wxITEM_NORMAL);
    Menu4->Append(MenuItem11);
    MenuItem7 = new wxMenuItem(Menu4, ID_MENUITEM5, _("Flip Left/Right"), _("Flip left/right"), wxITEM_NORMAL);
    Menu4->Append(MenuItem7);
    MenuItem8 = new wxMenuItem(Menu4, ID_MENUITEM6, _("Flip Top/Bottom"), _("Flip top/bottom"), wxITEM_NORMAL);
    Menu4->Append(MenuItem8);
    MenuBar1->Append(Menu4, _("Rotate"));
    Menu3 = new wxMenu();
    MenuItem3 = new wxMenuItem(Menu3, ID_MENUITEM1, _("Fit"), _("auto scale"), wxITEM_NORMAL);
    Menu3->Append(MenuItem3);
    MenuItem4 = new wxMenuItem(Menu3, ID_MENUITEM2, _("Zoom +"), _("Zoom in"), wxITEM_NORMAL);
    Menu3->Append(MenuItem4);
    MenuItem5 = new wxMenuItem(Menu3, ID_MENUITEM3, _("Zoom -"), _("Zoom out"), wxITEM_NORMAL);
    Menu3->Append(MenuItem5);
    MenuItem6 = new wxMenuItem(Menu3, ID_MENUITEM4, _("Scale 1:1"), _("Scale 1:1"), wxITEM_NORMAL);
    Menu3->Append(MenuItem6);
    MenuBar1->Append(Menu3, _("Zoom"));
    Menu2 = new wxMenu();
    MenuItem16 = new wxMenuItem(Menu2, ID_MENUITEM14, _("FIT Header "), wxEmptyString, wxITEM_NORMAL);
    Menu2->Append(MenuItem16);
    MenuItem2 = new wxMenuItem(Menu2, idMenuAbout, _("About\tF1"), _("Show info about this application"), wxITEM_NORMAL);
    Menu2->Append(MenuItem2);
    MenuBar1->Append(Menu2, _("Information"));
    SetMenuBar(MenuBar1);
    ToolBar1 = new wxToolBar(this, ID_TOOLBAR1, wxDefaultPosition, wxDefaultSize, wxTB_HORIZONTAL|wxNO_BORDER, _T("ID_TOOLBAR1"));
    ToolBarItem17 = ToolBar1->AddTool(IDB_QUIT, _("Quit"), wxBitmap(wxImage(_T("./toolbar/window-close.png"))), wxNullBitmap, wxITEM_NORMAL, _("Quit the application"), _("Quit the application"));
    ToolBarItem1 = ToolBar1->AddTool(IDB_OPEN, _("Open"), wxBitmap(wxImage(_T("./toolbar/document-open.png"))), wxNullBitmap, wxITEM_NORMAL, _("Open a FIT image"), _("Open a FIT image"));
    ToolBar1->AddSeparator();
    ToolBarItem2 = ToolBar1->AddTool(IDB_HOME, _("Home"), wxBitmap(wxImage(_T("./toolbar/arrow-left-home.png"))), wxNullBitmap, wxITEM_NORMAL, _("First image"), _("First image"));
    ToolBarItem3 = ToolBar1->AddTool(IDB_PREVIOUS, _("Previous"), wxBitmap(wxImage(_T("./toolbar/arrow-left.png"))), wxNullBitmap, wxITEM_NORMAL, _("Previous image"), _("Previous image"));
    ToolBarItem4 = ToolBar1->AddTool(IDB_NEXT, _("Next"), wxBitmap(wxImage(_T("./toolbar/arrow-right.png"))), wxNullBitmap, wxITEM_NORMAL, _("Next image"), _("Next image"));
    ToolBarItem5 = ToolBar1->AddTool(IDB_END, _("End"), wxBitmap(wxImage(_T("./toolbar/arrow-right-end.png"))), wxNullBitmap, wxITEM_NORMAL, _("Last image"), _("Last image"));
    ToolBar1->AddSeparator();
    ToolBarItem6 = ToolBar1->AddTool(IDB_PZOOM, _("Zoom in"), wxBitmap(wxImage(_T("./toolbar/zoom-in.png"))), wxNullBitmap, wxITEM_NORMAL, _("Zoom in"), _("Zoom in"));
    ToolBarItem7 = ToolBar1->AddTool(IDB_MZOOM, _("Zoom out"), wxBitmap(wxImage(_T("./toolbar/zoom-out.png"))), wxNullBitmap, wxITEM_NORMAL, _("Zoom out"), _("Zoom -out"));
    ToolBarItem8 = ToolBar1->AddTool(IDB_FIT, _("zoom fit"), wxBitmap(wxImage(_T("./toolbar/zoom-fit.png"))), wxNullBitmap, wxITEM_NORMAL, _("auto zoom"), _("auto zoom"));
    ToolBarItem9 = ToolBar1->AddTool(IDB_NOZOOM, _("NoZoom"), wxBitmap(wxImage(_T("./toolbar/zoom-0.png"))), wxNullBitmap, wxITEM_NORMAL, _("Scale 1:1"), _("Scale 1:1"));
    ToolBar1->AddSeparator();
    ToolBarItem15 = ToolBar1->AddTool(ID_TOOLBARITEM4, _("Auto Threshold"), wxBitmap(wxImage(_T("./toolbar/thresholdauto.png"))), wxNullBitmap, wxITEM_NORMAL, _("Automatic tuning of display thresholds"), _("Automatic tuning of display thresholds"));
    ToolBarItem16 = ToolBar1->AddTool(IDB_SEUIL, _("Manual threshold"), wxBitmap(wxImage(_T("./toolbar/threshold.png"))), wxNullBitmap, wxITEM_NORMAL, _("Manual tuning of display thresholds"), _("Manual tuning of display thresholds"));
    ToolBarItem24 = ToolBar1->AddTool(IDB_WB, _("White balance"), wxBitmap(wxImage(_T("./toolbar/whitebalance.png"))), wxNullBitmap, wxITEM_NORMAL, _("White balance"), _("White balance"));
    ToolBarItem12 = ToolBar1->AddTool(IDB_HISTOIMG, _("HistoImage"), wxBitmap(wxImage(_T("./toolbar/histo-img.png"))), wxNullBitmap, wxITEM_NORMAL, _("Histo Image"), _("Histo Image"));
    ToolBarItem18 = ToolBar1->AddTool(IDB_HISTOECRAN, _("Histogram Screen"), wxBitmap(wxImage(_T("./toolbar/histo-ecran.png"))), wxNullBitmap, wxITEM_NORMAL, _("Histo screen"), _("Histo screen"));
    ToolBarItem19 = ToolBar1->AddTool(IDB_HISTOPREF, _("Histogram Pref"), wxBitmap(wxImage(_T("./toolbar/histo-pref.png"))), wxNullBitmap, wxITEM_NORMAL, _("Histogram Linear/Log"), _("Histogram Linear/Log"));
    ToolBarItem11 = ToolBar1->AddTool(ID_TOOLBARITEM1, _("FitHeader"), wxBitmap(wxImage(_T("./toolbar/header-img.png"))), wxNullBitmap, wxITEM_NORMAL, _("FIT Header"), _("FIT header"));
    ToolBarItem10 = ToolBar1->AddTool(IDB_STAT_IMAGE, _("Statistic"), wxBitmap(wxImage(_T("./toolbar/Stat-img.png"))), wxNullBitmap, wxITEM_NORMAL, _("Image Statistic"), _("Image Statistic"));
    ToolBar1->AddSeparator();
    ToolBarItem20 = ToolBar1->AddTool(IDB_ROTATELEFT, _("RotateLeft"), wxBitmap(wxImage(_T("./toolbar/img-rotate-left.png"))), wxNullBitmap, wxITEM_NORMAL, _("Left rotate of the image"), _("Left rotate of the image"));
    ToolBarItem21 = ToolBar1->AddTool(IDB_ROTATERIGHT, _("Rotate Right"), wxBitmap(wxImage(_T("./toolbar/img-rotate-right.png"))), wxNullBitmap, wxITEM_NORMAL, _("Right rotate of the image"), _("Right rotate of the image"));
    ToolBarItem22 = ToolBar1->AddTool(IDB_FLIPH, _("Flip Left/Right"), wxBitmap(wxImage(_T("./toolbar/img-flip-h.png"))), wxNullBitmap, wxITEM_NORMAL, _("Flip Left/Right"), _("Flip  Left/Right"));
    ToolBarItem23 = ToolBar1->AddTool(IDB_FLIPV, _("Flip Top/bottom"), wxBitmap(wxImage(_T("./toolbar/img-flip-v.png"))), wxNullBitmap, wxITEM_NORMAL, _("Flip top/bottom"), _("Flip top/bottom"));
    ToolBar1->AddSeparator();
    ToolBarItem13 = ToolBar1->AddTool(IDB_MENU, _("Menu"), wxBitmap(wxImage(_T("./toolbar/Menu_toggle.png"))), wxNullBitmap, wxITEM_NORMAL, _("Menu"), _("Menu"));
    ToolBarItem14 = ToolBar1->AddTool(ID_TOOLBARITEM3, _("About"), wxBitmap(wxImage(_T("./toolbar/help-about.png"))), wxNullBitmap, wxITEM_NORMAL, _("About"), _("About"));
    ToolBar1->Realize();
    SetToolBar(ToolBar1);

    Connect(ID_OPEN_FIT,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnOpenFit);
    Connect(idMenuQuit,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnQuit);
    Connect(ID_NEXT,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnNextImage);
    Connect(ID_PREC,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnPreviousImage);
    Connect(ID_FIRST,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnFirstImage);
    Connect(ID_LAST,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnLastImage);
    Connect(ID_MENUITEM8,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnSeuil);
    Connect(ID_MENUITEM15,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnSeuilAuto);
    Connect(ID_WB,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnWhiteBalance);
    Connect(ID_MENUITEM10,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnStatistic);
    Connect(ID_MENUITEM11,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnHistogram);
    Connect(ID_MENUITEM12,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnHistogramVisu);
    Connect(ID_MENUITEM13,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnLineaireLog);
    Connect(ID_MENUITEM7,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnRot90);
    Connect(ID_MENUITEM9,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnRot270);
    Connect(ID_MENUITEM5,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnFlipH);
    Connect(ID_MENUITEM6,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnFlipV);
    Connect(ID_MENUITEM1,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnFitImage);
    Connect(ID_MENUITEM2,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnZoomPlus);
    Connect(ID_MENUITEM3,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnZoomMoins);
    Connect(ID_MENUITEM4,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnEchelle1);
    Connect(ID_MENUITEM14,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnHeaderInfo);
    Connect(idMenuAbout,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&vfitFrame::OnAbout);
    Connect(IDB_QUIT,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnQuit);
    Connect(IDB_OPEN,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnOpenFit);
    Connect(IDB_HOME,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnFirstImage);
    Connect(IDB_PREVIOUS,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnPreviousImage);
    Connect(IDB_NEXT,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnNextImage);
    Connect(IDB_END,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnLastImage);
    Connect(IDB_PZOOM,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnZoomPlus);
    Connect(IDB_MZOOM,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnZoomMoins);
    Connect(IDB_FIT,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnFitImage);
    Connect(IDB_NOZOOM,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnEchelle1);
    Connect(ID_TOOLBARITEM4,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnSeuilAuto);
    Connect(IDB_SEUIL,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnSeuil);
    Connect(IDB_WB,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnWhiteBalance);
    Connect(IDB_HISTOIMG,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnHistogram);
    Connect(IDB_HISTOECRAN,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnHistogramVisu);
    Connect(IDB_HISTOPREF,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnLineaireLog);
    Connect(ID_TOOLBARITEM1,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnHeaderInfo);
    Connect(IDB_STAT_IMAGE,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnStatistic);
    Connect(IDB_ROTATELEFT,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnRot270);
    Connect(IDB_ROTATERIGHT,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnRot90);
    Connect(IDB_FLIPH,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnFlipH);
    Connect(IDB_FLIPV,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnFlipV);
    Connect(IDB_MENU,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnVisibleMenu);
    Connect(ID_TOOLBARITEM3,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&vfitFrame::OnAbout);
    //*)

    m_SeuilDlg = new CSeuilDialog(parent)     ;
    m_InfoDlg  = new CInfoDialog(parent)      ;
    m_HistoDlg = new CHistogramDialog(parent) ;

    m_SeuilDlg->setHistoDialog(m_HistoDlg);

    wxIcon FrameIcon;
    vfit_bitmap.LoadFile(wxT("./toolbar/vfit.png"), wxBITMAP_TYPE_PNG);
    FrameIcon.CopyFromBitmap(vfit_bitmap);
    SetIcon(FrameIcon);

    m_info       =wxEmptyString ;
    m_parent     = parent       ;

    m_menubar    = MenuBar1 ;
    m_nTotalFit  = 0        ;
    m_nCurFit    = 65535    ;
    m_HistoEcran = false    ;
    m_toggleWB   = 0        ;

    wxPlatformInfo infosys ;
    if (  infosys.GetOperatingSystemId() == wxOS_UNIX_LINUX ) // wxOS_WINDOWS
    {
        MenuBar1->Hide() ;
        m_toggleMenu = 0 ;
    }
    else
    {
        ToolBar1->DeleteTool( IDB_MENU );
    }

    wxSetWorkingDirectory( cwd );
}

vfitFrame::~vfitFrame()
{
    //(*Destroy(vfitFrame)
    //*)

    delete m_SeuilDlg   ;
    delete m_ImagePanel ;
    delete m_InfoDlg    ;
    delete m_HistoDlg   ;
}

void vfitFrame::OnAbout(wxCommandEvent& event)
{
    wxArrayString translators ;
    wxIcon myIcon;
    myIcon.CopyFromBitmap(vfit_bitmap);

    wxAboutDialogInfo info;
    info.SetIcon(myIcon);
    info.SetName(_("VFIT"));
    info.SetVersion(_("0.1.0"));
    info.SetDescription(_("This program is a viewer of FIT image\n(Grayscale and RGB  [8,16,32bits] format only)"));
    info.SetCopyright(_("(C) 27/12/2017 M27trognondepomme <pebe92@gmail.com>"));
    info.SetLicense(_("This program is provided without any guarantee.\n"
                        "For details, see GNU General Public License, version 3 or later.\n"
                        "https://www.gnu.org/licenses/gpl.html"));
    info.AddTranslator(_("M27trognondepomme (fr_Fr)"));
    info.AddDeveloper(_("M27trognondepomme"));
    wxAboutBox(info);
}

void vfitFrame::OnQuit(wxCommandEvent& event)
{
    Close();
}


bool vfitFrame::LoadImage( wxString filename )
{
    wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
    m_ImagePanel = new ScrolledImagePanel( this,  wxID_ANY,filename );

    if ( ! m_ImagePanel->isOk())
        return false ;

    sizer->Add(m_ImagePanel, 1, wxEXPAND );
    this->SetSizer(sizer);

    InitSeuil() ;
    m_fitname = filename ;
    GetIndexFit() ;

    return true ;
}


void vfitFrame::OnSeuilAuto(wxCommandEvent& event)
{
    m_ImagePanel->CalcStat() ;

    CImg    *img        = m_ImagePanel->GetImgFit() ;
    uint16_t bitpix     = img->getBitPix() ;
    uint32_t dynamique  = (1LL<<bitpix)-1 ;
    float    range      = (float) (dynamique) ;
    float    vhaut      = m_ImagePanel->m_stat.mean[0] + 3.f*m_ImagePanel->m_stat.stddev[0] ;
    float    vbas       = m_ImagePanel->m_stat.mean[0] - 3.f*m_ImagePanel->m_stat.stddev[0] ;

    vhaut = (vhaut > m_ImagePanel->m_stat.max[0]) ? m_ImagePanel->m_stat.max[0] : vhaut ;
    vbas  = (vbas  < m_ImagePanel->m_stat.min[0]) ? m_ImagePanel->m_stat.min[0] : vbas  ;
    vbas  = (vbas < 0) ? 0.0 : vbas ;

    uint32_t haut       = static_cast<uint32_t>( vhaut*range) ;
    uint32_t bas        = static_cast<uint32_t>( vbas*range) ;

    if ( abs((int) bitpix ) > 16 )
    {
        haut >>= 16 ;
        bas  >>= 16 ;
    }

    m_SeuilDlg->setSeuil(bas,haut) ;
}

void vfitFrame::OnFitImage(wxCommandEvent& event)
{
    m_ImagePanel->setZoom(0.0) ;
}

void vfitFrame::OnEchelle1(wxCommandEvent& event)
{
    m_ImagePanel->setZoom(1.0) ;
}

void vfitFrame::OnZoomPlus(wxCommandEvent& event)
{
    m_ImagePanel->setZoom(2.0) ;
}

void vfitFrame::OnZoomMoins(wxCommandEvent& event)
{
    m_ImagePanel->setZoom(-2.0) ;
}

void vfitFrame::OnFlipV(wxCommandEvent& event)
{
    m_ImagePanel->FlipImage(false) ;
}

void vfitFrame::OnFlipH(wxCommandEvent& event)
{
    m_ImagePanel->FlipImage(true) ;
}

void vfitFrame::OnRot90(wxCommandEvent& event)
{
    m_ImagePanel->RotImage(1) ;
}

void vfitFrame::OnRot270(wxCommandEvent& event)
{
    m_ImagePanel->RotImage(3) ;
}

void vfitFrame::OnSeuil(wxCommandEvent& event)
{
    m_SeuilDlg->Show() ;
}

void vfitFrame::FormatDisplayStatistic( wxString label, bool rgb, float *value , float range)
{
    float vfloat = value[0] * range ;
    int   q      = range== 1.0 ? 10 : 1              ;
    m_info << wxT("  + ") << label << wxT(" = ") << wxNumberFormatter::ToString(vfloat,q) << wxT("\n") ;
    if ( rgb )
        for ( uint16_t ii=1; ii<= 3 ; ii++ )
        {
            vfloat = value[ii]  * range ;
            m_info << wxT("     o ") << label << wxT(" Channel")<< ii << wxT(" = ") << wxNumberFormatter::ToString(vfloat,q) << wxT("\n") ;
        }
}
void vfitFrame::OnStatistic(wxCommandEvent& event)
{
    if ( m_info.IsEmpty())
    {
        m_ImagePanel->CalcStat() ;

        bool  rgb      = m_ImagePanel->m_stat.rgb          ;
        float range    = m_ImagePanel->m_stat.range        ;
        int   q        = (range == 1.0 ) ? 10 : 1          ;
        CImg  *img     =  m_ImagePanel->GetImgFit()        ;

        m_info=wxT("Image : ");
        m_info << m_fitname << wxT("\n") ;
        m_info << wxT("  + Resolution =")          << img->getColonne() << wxT(" x ") << img->getLigne() << wxT("\n");
        m_info << wxT("  + pixels =")              << img->getColonne() * img->getLigne()  << wxT("\n");
        m_info << wxT("  + Plan Number = ")        << img->getPlan()                       << wxT(" (1:grayscale/3:RVB)\n")  ;
        m_info << wxT("  + Coding Bit Number  = ") << (int) img->getBitPix()               << wxT(" bits\n") ;
        m_info << wxT("  +  range = 0 - ")         << wxNumberFormatter::ToString(range,q) << wxT("\n\n") ;

        FormatDisplayStatistic( wxT("Max")      ,rgb,m_ImagePanel->m_stat.max   ,m_ImagePanel->m_stat.range) ;
        FormatDisplayStatistic( wxT("Min")      ,rgb,m_ImagePanel->m_stat.min   ,m_ImagePanel->m_stat.range) ;
        FormatDisplayStatistic( wxT("Mean")     ,rgb,m_ImagePanel->m_stat.mean  ,m_ImagePanel->m_stat.range) ;
        FormatDisplayStatistic( wxT("Deviation"),rgb,m_ImagePanel->m_stat.stddev,m_ImagePanel->m_stat.range) ;
        FormatDisplayStatistic( wxT("Median")   ,rgb,m_ImagePanel->m_stat.median,m_ImagePanel->m_stat.range) ;
    }

    m_InfoDlg->SetText(m_info) ;
    m_InfoDlg->ShowModal() ;
}

void vfitFrame::OnHistogram(wxCommandEvent& event)
{
    m_HistoDlg->Show() ;
    m_HistoEcran = false ;

    CImg *img = m_ImagePanel->GetImgFit() ;

    uint32_t bin = 256 ;
    uint32_t *r_hist = nullptr ;
    uint32_t *v_hist = nullptr ;
    uint32_t *b_hist = nullptr ;

    uint32_t *l_hist = img->ArrayHistogram(0,bin) ;
    if ( img->getPlan() == 3 )
    {
        r_hist = img->ArrayHistogram(1,bin) ;
        v_hist = img->ArrayHistogram(2,bin) ;
        b_hist = img->ArrayHistogram(3,bin) ;
    }

    m_HistoDlg->SetTitle( _("Image Histogram") );
    m_HistoDlg->SetHistogram( bin, l_hist, r_hist, v_hist, b_hist);

    if (l_hist!= nullptr ) delete [] l_hist ;
    if (r_hist!= nullptr ) delete [] r_hist ;
    if (v_hist!= nullptr ) delete [] v_hist ;
    if (b_hist!= nullptr ) delete [] b_hist ;
}

void vfitFrame::OnHistogramVisu(wxCommandEvent& event)
{
    m_HistoDlg->Show() ;
    m_HistoEcran = true ;
    m_SeuilDlg->RefreshHistogramVisu() ;
}

void vfitFrame::OnLineaireLog(wxCommandEvent& event)
{
    m_HistoDlg->ToggleMode() ;
    if ( m_HistoEcran )
        m_SeuilDlg->RefreshHistogramVisu() ;
    else
        OnHistogram(event) ;

}

void vfitFrame::OnHeaderInfo(wxCommandEvent& event)
{
    char buffer[SZ_KW+1];
    wxString header(wxT("Header: "));
    std::ifstream  ifs    ;
    ifs.open(m_fitname, std::ios::binary);
    header << m_fitname << wxT("\n") ;
    while (1)
    {
        ifs.read(buffer, SZ_KW ) ;
        buffer[SZ_KW]=0;
        header << buffer << wxT("\n") ;
        if (strncmp(buffer , "END     ",SZ_NKW) == 0)
        {
            break ;
        }
    }
    m_InfoDlg->SetText(header) ;
    m_InfoDlg->ShowModal() ;

}

void vfitFrame::OnOpenFit(wxCommandEvent& event)
{
    wxFileDialog openFileDialog(0, wxT("Display a FIT file") ,
            wxEmptyString , wxEmptyString, wxT("fit|*.fit"),
            wxFD_OPEN | wxFD_FILE_MUST_EXIST );

    if (openFileDialog.ShowModal() == wxID_OK)
    {
        OpenImage( openFileDialog.GetPath() );
    }
}

bool vfitFrame::OpenImage( wxString fichierFit )
{
    delete m_HistoDlg   ;
    m_info = wxEmptyString ;
    m_HistoDlg = new CHistogramDialog(m_parent) ;
    m_SeuilDlg->setHistoDialog(m_HistoDlg);

    m_ImagePanel->open(fichierFit);
    InitSeuil() ;
    m_fitname = fichierFit ;
    SetTitle(m_fitname);
    GetIndexFit() ;

    return true ;
}

void vfitFrame::InitSeuil()
{
    CImg    *img        = m_ImagePanel->GetImgFit() ;
    uint16_t bitpix     = img->getBitPix() ;
    uint32_t dynamique  = (1LL<<bitpix)-1 ;
    float    vmax       = img->getVisuHaut() ;
    float    vmin       = img->getVisuBas()  ;
    if ( vmax<vmin )
    {
        vmax       = m_ImagePanel->getMax() ;
        vmin       = m_ImagePanel->getMin() ;
    }
    float    range      = (float) (dynamique) ;
    vmin                = (vmin < 0) ? 0.0 : vmin ;
    uint32_t haut       = static_cast<uint32_t>( vmax*range) ;
    uint32_t bas        = static_cast<uint32_t>( vmin*range) ;

    if ( abs((int) bitpix ) > 16 )
    {
        haut >>= 16 ;
        bas  >>= 16 ;
        dynamique >>=16 ;
    }

    m_toggleWB   = 0        ;

    m_SeuilDlg->setImagePanel(m_ImagePanel);
    m_SeuilDlg->setRange(0,dynamique);
    m_SeuilDlg->setSeuil(bas,haut) ;
}

void vfitFrame::GetIndexFit()
{
    wxFileName file = wxFileName(m_fitname) ;
    wxString   path = file.GetPath() ;
    if ( path == wxEmptyString )
        path = ".";
    wxDir dir(path);

    wxArrayString dirList;
    dir.GetAllFiles(dir.GetName(), &dirList, "*.fit", wxDIR_FILES);
    dirList.Shrink();
    dirList.Sort(false);
    m_nTotalFit = dirList.GetCount();
    for (size_t ii = 0; ii < dirList.GetCount(); ii++)
    {
        wxString filename = wxFileName(dirList.Item(ii)).GetFullName() ;
        if ( filename.Cmp(file.GetFullName())==0)
            m_nCurFit =ii ;
    }

}

void vfitFrame::LoadIndexFit(uint16_t no_fit)
{
    wxFileName file = wxFileName(m_fitname) ;
    wxString   path = file.GetPath() ;
    if ( path == wxEmptyString )
        path = ".";
    wxDir dir(path);

    wxArrayString dirList;
    dir.GetAllFiles(dir.GetName(), &dirList, "*.fit", wxDIR_FILES);
    dirList.Shrink();
    dirList.Sort(false);
    if ( dirList.GetCount() == 0 )
         return ;
    if ( no_fit >= dirList.GetCount())
         no_fit = dirList.GetCount()-1 ;

    OpenImage( dirList.Item(no_fit) ) ;
}

void vfitFrame::OnNextImage(wxCommandEvent& event)
{
    uint16_t next = m_nCurFit + 1 ;
    if (next>=m_nTotalFit)
       next = 0 ;
    LoadIndexFit(next);
}

void vfitFrame::OnPreviousImage(wxCommandEvent& event)
{
    uint16_t prev = m_nCurFit  ;
    if (prev==0)
        prev = m_nTotalFit ;
    prev--;
    LoadIndexFit(prev);
}

void vfitFrame::OnFirstImage(wxCommandEvent& event)
{
    LoadIndexFit(0);
}

void vfitFrame::OnLastImage(wxCommandEvent& event)
{
    LoadIndexFit(m_nTotalFit-1);
}

void vfitFrame::OnVisibleMenu(wxCommandEvent& event)
{
    m_toggleMenu = 1-m_toggleMenu ;
    if ( m_toggleMenu )
        m_menubar->Show();
    else
        m_menubar->Hide();
}

void vfitFrame::OnWhiteBalance(wxCommandEvent& event)
{
   m_ImagePanel->CalcStat() ;
   if ( m_ImagePanel->m_stat.rgb == false )
        return ;

    if ( m_toggleWB == 0 )
    {

        float median_max = m_ImagePanel->m_stat.median[1] ;
        if ( m_ImagePanel->m_stat.median[2] > median_max ) median_max = m_ImagePanel->m_stat.median[2] ;
        if ( m_ImagePanel->m_stat.median[3] > median_max ) median_max = m_ImagePanel->m_stat.median[3] ;

        m_ImagePanel->setWhiteBalance(   median_max/m_ImagePanel->m_stat.median[1],
                                         median_max/m_ImagePanel->m_stat.median[2],
                                         median_max/m_ImagePanel->m_stat.median[3]);
    }
    else
    {
        m_SeuilDlg->RefreshSeuil();
    }

    m_toggleWB = 1 - m_toggleWB ;
}
