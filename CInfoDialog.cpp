#include "CInfoDialog.h"

//(*InternalHeaders(CInfoDialog)
#include <wx/string.h>
#include <wx/intl.h>
//*)

//(*IdInit(CInfoDialog)
const long CInfoDialog::ID_TEXTCTRL1 = wxNewId();
const long CInfoDialog::ID_BUTTON1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(CInfoDialog,wxDialog)
	//(*EventTable(CInfoDialog)
	//*)
END_EVENT_TABLE()

CInfoDialog::CInfoDialog(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size)
{
	//(*Initialize(CInfoDialog)
	wxBoxSizer* BoxSizer1;

	Create(parent, id, _("Information"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE, _T("id"));
	SetClientSize(wxDefaultSize);
	Move(wxDefaultPosition);
	BoxSizer1 = new wxBoxSizer(wxVERTICAL);
	TextCtrl1 = new wxTextCtrl(this, ID_TEXTCTRL1, _("Text"), wxDefaultPosition, wxSize(300,300), wxTE_MULTILINE|wxTE_READONLY|wxTE_DONTWRAP|wxVSCROLL|wxHSCROLL, wxDefaultValidator, _T("ID_TEXTCTRL1"));
	BoxSizer1->Add(TextCtrl1, 1, wxALL|wxEXPAND, 5);
	Button1 = new wxButton(this, ID_BUTTON1, _("OK"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON1"));
	Button1->SetDefault();
	BoxSizer1->Add(Button1, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	SetSizer(BoxSizer1);
	BoxSizer1->Fit(this);
	BoxSizer1->SetSizeHints(this);

	Connect(ID_BUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&CInfoDialog::OnButton1Click);
	//*)
}

CInfoDialog::~CInfoDialog()
{
	//(*Destroy(CInfoDialog)
	//*)
}


void CInfoDialog::OnButton1Click(wxCommandEvent& event)
{
    Close() ;
}

void CInfoDialog::SetText( wxString value )
{
    TextCtrl1->SetValue(value) ;
}
