#ifndef CHISTOPANEL_H
#define CHISTOPANEL_H

#include <wx/panel.h>
#include <wx/sizer.h>


class CHistoPanel: public wxPanel
{
    public:
        CHistoPanel(wxWindow* parent,wxWindowID id=wxID_ANY,const wxPoint& pos=wxDefaultPosition,const wxSize& size=wxDefaultSize);

        void paintEvent(wxPaintEvent & evt);
        void paintNow();
        void render(wxDC& dc);

        void setBitmap( wxBitmap *bmp ) ;
        // some useful events
        /*
         void mouseMoved(wxMouseEvent& event);
         void mouseDown(wxMouseEvent& event);
         void mouseWheelMoved(wxMouseEvent& event);
         void mouseReleased(wxMouseEvent& event);
         void rightClick(wxMouseEvent& event);
         void mouseLeftWindow(wxMouseEvent& event);
         void keyPressed(wxKeyEvent& event);
         void keyReleased(wxKeyEvent& event);
         */
    protected:
        wxBitmap *m_bitmap ;

    private:
		DECLARE_EVENT_TABLE()
};

#endif /* CHISTOPANEL_H */
