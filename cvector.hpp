#include    <stdint.h>
#include    <cstdlib>
#include    <iostream>

#ifndef CVECTOR_HPP
#define CVECTOR_HPP

template <typename T>
class CVector {

public:
    CVector( const CVector<T>& x ) ; // constructeur de copie
    CVector() ;
    CVector( uint32_t nb_items, T value=0 ) ;
    CVector( uint32_t nb_items, T *array  ) ;
    ~CVector() ;

    uint32_t get_items() const { return m_items ; } ;
    const T *get_array() const { return m_data ;  } ;

    CVector<T>& operator = (const CVector<T>& x);
    CVector<T>& operator+= (const CVector<T>& x);
    CVector<T>& operator-= (const CVector<T>& x);
    
    CVector<T>& operator+= (T x);
    CVector<T>& operator-= (T x);
    CVector<T>& operator*= (T x);
    CVector<T>& operator/= (T x);


    T & operator[](uint32_t ii) const ;
    T & operator[](uint32_t ii)  ;

    void fill( T value=0 ) ;
    void fill( uint32_t nb_items , const T *array )  ;

    T min(  ) ;
    T max(  ) ;
    
    template <class U>
    friend std::ostream& operator<<(std::ostream& s, const CVector<U> & x);

protected:
    void destroy() ;
    void create(uint32_t nItems ) ;
    
    uint32_t m_items ;
    T     *m_data  ;

};

// -----------------------------------------------------------------------------

typedef CVector<int8_t>  vector_i8_t ;
typedef CVector<uint8_t> vector_u8_t ;

typedef CVector<int16_t>  vector_i16_t ;
typedef CVector<uint16_t> vector_u16_t ;

typedef CVector<int32_t>  vector_i32_t ;
typedef CVector<uint32_t> vector_u32_t ;

typedef CVector<float> vector_f_t ;

// -----------------------------------------------------------------------------

template <typename T>
inline CVector<T> operator + ( const CVector<T> & oper1 , const CVector<T> & oper2 ) 
{
    CVector<T> r(oper1) ;
    r+=oper2 ;
    
    return r ;
}

template <typename T>
inline CVector<T> operator - ( const CVector<T> & oper1 , const CVector<T> & oper2 ) 
{
    CVector<T> r(oper1) ;
    r-=oper2 ;
    
    return r ;
}

// -----------------------------------------------------------------------------

template <typename T>
void CVector<T>::destroy()
{
    if ( m_data != nullptr ) 
        delete [] m_data ; 
    m_items = 0 ;
    m_data  = nullptr ;
}

template <typename T>
void CVector<T>::create(uint32_t nItems ) 
{
    try
    {
        destroy() ;

        if ( nItems != 0 )
        {
            m_data = new  T [nItems] ;
            if ( m_data == nullptr )
                throw( "erreur allocation Cvector<>") ;

            m_items = nItems ;
        }
    }
    catch (const char *err)
    {
        fprintf(stderr, "%s\n", err); 
    }
}

template <typename T>
void CVector<T>::fill( T value )
{
    for ( uint32_t ii=0; ii<m_items; ii++)
        m_data[ii] = value ;
}

template <typename T>
void CVector<T>::fill( uint32_t nb_items , const T *array )
{
    uint32_t nb_loop = std::min( nb_items , m_items ) ;
    for ( uint32_t ii=0; ii<nb_loop; ii++)
        m_data[ii] = array[ii] ;
}
// -----------------------------------------------------------------------------
// constructeurs et destructeur

template <typename T>
CVector<T>::~CVector() 
{
    destroy() ;
}

template <typename T>
CVector<T>::CVector() : m_items(0),  m_data(nullptr)
{
}

template <typename T>
CVector<T>::CVector( uint32_t nb_items ,  T  value)
{
    m_data  = nullptr ;    
    create( nb_items ) ;
    fill( value ) ;
}

template <typename T>
CVector<T>::CVector(uint32_t nb_items , T  *array): m_data(nullptr)
{
    m_data  = nullptr ;
    create( nb_items ) ;

    if ( array == nullptr )
        fill(0) ;
    else
        fill(nb_items,array) ;
}
// -----------------------------------------------------------------------------
// Copie

template <typename T>
CVector<T>::CVector(const CVector<T>& x)
{
    m_data  = nullptr ;
    create( x.get_items()) ;
    fill(x.get_items(),x.get_array());    
}

// -----------------------------------------------------------------------------

template <typename T>
T & CVector<T>::operator[](uint32_t ii) const 
{
    if ( ( ii < 0 ) ||  ( ii > m_items ) )
    {
        std::cerr <<  "index out of range" << '\n'  ;
        std::exit(1) ;
    }
    return m_data[ii] ;
}

template <typename T>
T & CVector<T>::operator[](uint32_t ii) 
{
    if ( ( ii < 0 ) ||  ( ii > m_items ) )
    {
        std::cerr <<  "index out of range" << '\n'  ;
        std::exit(1) ;
    }
    return m_data[ii] ;
}

template <typename T>
std::ostream& operator<<(std::ostream& s , const CVector<T> &  x)
{
    s << "("<<x.get_items() <<")[" ;
    if ( x.get_items() > 0  )
    {
        for ( uint32_t ii=0; ii<x.get_items()-1; ii++)
            s << (T) x[ii] << ',' ;
            
        s << (T) x[x.get_items()-1] ;        
    }
    s <<  " ]" ; 
    
    return s ;
}
    
template <typename T>
CVector<T> & CVector<T>::operator= (const CVector<T>& x)
{
    if ( m_items != x.get_items() )
        destroy() ;
    
    m_data = new T [x.get_items()] ;
    if ( m_data == 0 ) 
        return *this;
        
    const T *x_data=x.get_array() ;
    m_items = x.get_items() ;
    for ( uint32_t ii=0; ii<m_items; ii++)
        m_data[ii] = x_data[ii] ;

    
    return *this ;    
}

template <typename T>
CVector<T> & CVector<T>::operator+= (const CVector<T>& x)
{
    uint32_t items = x.get_items() ;
    if ( m_items < items )
        items = m_items ;
        
    const T *x_data=x.get_array() ;
        
    for ( uint32_t ii=0; ii<items; ii++)
        m_data[ii] += x_data[ii] ;
        
    return *this ;    
}    

template <typename T>
CVector<T> & CVector<T>::operator-= (const CVector<T>& x)
{
    uint32_t items = x.get_items() ;
    if ( m_items < items )
        items = m_items ;
        
    const T *x_data=x.get_array() ;
    for ( uint32_t ii=0; ii<items; ii++)
        m_data[ii] -= x_data[ii] ;
    
    return *this ;    
}  

template <typename T>
CVector<T> & CVector<T>::operator+= (T x)
{   
    for ( uint32_t ii=0; ii<m_items; ii++)
        m_data[ii] += x ;
        
    return *this ;    
}    

template <typename T>
CVector<T> & CVector<T>::operator-= (T x)
{
    for ( uint32_t ii=0; ii<m_items; ii++)
        m_data[ii] -= x ;
    
    return *this ;    
}  

template <typename T>
CVector<T> & CVector<T>::operator*= (T x)
{   
    for ( uint32_t ii=0; ii<m_items; ii++)
        m_data[ii] *= x ;
        
    return *this ;    
}    

template <typename T>
CVector<T> & CVector<T>::operator/= (T x)
{
    for ( uint32_t ii=0; ii<m_items; ii++)
        m_data[ii] /= x ;
    
    return *this ;    
}


template <typename T>
T CVector<T>::max( )
{
    T v_max = m_data[0];
    for (uint32_t ii = 1; ii < m_items ; ii++)
    {
        v_max = (v_max>=m_data[ii] )? v_max:m_data[ii]  ;
    }

    return v_max ;
}

template <typename T>
T CVector<T>::min(  )
{
    T v_min = m_data[0];
    for (uint32_t ii = 0; ii < m_items ; ii++)
    {
        v_min=(v_min<=m_data[ii] )? v_min:m_data[ii] ;
    }
    return v_min ;
}


#endif /* CVECTOR_HPP */

