#include <wx/wx.h>
#include <wx/sizer.h>


#ifndef WXSCROLLIMAGE_HPP
#define WXSCROLLIMAGE_HPP

#include "cimg.hpp"


class ScrolledImagePanel : public wxScrolledWindow
{
    CImg       *m_imgfit    ;
    float       m_zoom      ;
    wxImage     m_image     ;
    wxBitmap    m_bitmap    ;
    int         m_w ,m_h    ;
    float       m_ratio_w_h ;
    bool        m_fileGood  ;

    void FitImage(int w_DC, int h_DC)
    {
        int neww, newh;
        int marge = 20 ;

        neww = (int) ( (float) (h_DC-marge) * m_ratio_w_h );
        newh = (int) ( (float) (w_DC-marge) / m_ratio_w_h  );

        if ( newh > h_DC )
        {
            newh =  h_DC - marge ;
        }
        if ( neww > w_DC )
        {
            neww = w_DC-marge ;
        }
        m_w = neww ;
        m_h = newh ;
    }

public:
    struct {
        bool  processed ;
        bool  rgb       ;
        float range     ;
        float max[4]    ;
        float min[4]    ;
        float mean[4]   ;
        float stddev[4] ;
        float median[4] ;
    } m_stat;

    bool isOk() { return m_fileGood ; }

    ScrolledImagePanel(wxWindow* parent, wxWindowID id, wxString image_path) : wxScrolledWindow(parent, id)
    {
        m_imgfit = new CImg();
        if ( ! m_imgfit->readFIT( image_path.c_str() ) )
        {
            wxLogError("format FIT incorrect =>'%s'.", image_path );
            m_fileGood = false ;
            return ;
        }

        m_fileGood = true ;

        m_stat.processed = false  ;

        uint16_t hh  = m_imgfit->getLigne() ;
        uint16_t ww  = m_imgfit->getColonne() ;

        if ( (hh > 0) && (ww > 0))
        {
            uint8_t *rgbArray = m_imgfit->ArrayRGB8(0.0,1.0) ;

            m_image.SetData(rgbArray,ww,hh,false);

            m_w = m_image.GetWidth();
            m_h = m_image.GetHeight();
            m_ratio_w_h = (float) m_image.GetWidth() / (float) m_image.GetHeight() ;

            FitImage(320,240) ;
            SetScrollbars(1,1, m_w, m_h, 0, 0);

            m_zoom = 0.0 ;
            this->Connect(wxEVT_SIZE,(wxObjectEventFunction)&ScrolledImagePanel::OnSize,0,this);
        }

    }
    ~ScrolledImagePanel()
    {
        delete m_imgfit ;
    }

    void open(wxString image_path)
    {
        CImg  *imgfit = new CImg();
        if ( ! imgfit->readFIT( image_path.c_str() ) )
        {
            wxLogError("format FIT incorrect =>'%s'.", image_path );
            m_fileGood = false ;
            return ;
        }

        m_fileGood = true ;

        delete  m_imgfit ;
        m_imgfit =  imgfit ;

        m_stat.processed = false  ;

        uint16_t hh  = m_imgfit->getLigne() ;
        uint16_t ww  = m_imgfit->getColonne() ;

        if ( (hh > 0) && (ww > 0))
        {
            uint8_t *rgbArray = m_imgfit->ArrayRGB8(0.0,1.0) ;

            m_image.SetData(rgbArray,ww,hh,false);

            m_w = m_image.GetWidth();
            m_h = m_image.GetHeight();
            m_ratio_w_h = (float) m_image.GetWidth() / (float) m_image.GetHeight() ;

            FitImage(320,240) ;
            SetScrollbars(1,1, m_w, m_h, 0, 0);

            m_zoom = 0.0 ;
        }
    }

    float getMax() { return m_imgfit->max() ; }
    float getMin() { return m_imgfit->min() ; }

    void CalcStat()
    {
        bool isFloat       = m_imgfit->isFloat() ;
        uint16_t bitpix    = m_imgfit->getBitPix() ;
        uint16_t nplan     = m_imgfit->getPlan() ;
        uint32_t dynamique = (1LL<<bitpix)-1 ;

        if (m_stat.processed)
            return ;

        m_stat.rgb   = ( nplan>1 ) ;
        m_stat.range = (float) (dynamique) ;
        if ( isFloat )
            m_stat.range = 1.0f ;

        m_stat.max[0] = m_imgfit->max() ;
        if ( m_stat.rgb )
            for ( uint16_t ii=1; ii<= nplan ; ii++ )
            {
                 m_stat.max[ii] = m_imgfit->max(ii) ;
            }

        m_stat.min[0] = m_imgfit->min() ;
        if ( m_stat.rgb )
            for ( uint16_t ii=1; ii<= nplan ; ii++ )
            {
                 m_stat.min[ii] = m_imgfit->min(ii) ;
            }

        m_stat.mean[0] = m_imgfit->mean() ;
        if ( m_stat.rgb )
            for ( uint16_t ii=1; ii<= nplan ; ii++ )
            {
                 m_stat.mean[ii] = m_imgfit->mean(ii) ;
            }

        m_stat.stddev[0] = m_imgfit->stddev(m_stat.mean[0]) ;
        if ( m_stat.rgb )
            for ( uint16_t ii=1; ii<= nplan ; ii++ )
            {
                 m_stat.stddev[ii] = m_imgfit->stddev(m_stat.mean[ii],ii) ;
            }

        m_stat.median[0] = m_imgfit->median() ;
        if ( m_stat.rgb )
            for ( uint16_t ii=1; ii<= nplan ; ii++ )
            {
                 m_stat.median[ii] = m_imgfit->median(ii) ;
            }

        m_stat.processed = true ;
    }

    CImg *GetImgFit() { return m_imgfit ; }

    void OnDraw(wxDC& dc)
    {
        int w_DC, h_DC;
        dc.GetSize( &w_DC, &h_DC );

        if ( m_zoom == 0.0 )
        {
            FitImage(w_DC,h_DC) ;
        }

        m_bitmap = wxBitmap( m_image.Scale( m_w, m_h ) );
        int offset_w = (w_DC - m_bitmap.GetWidth())/2 ;
        int offset_h = (h_DC - m_bitmap.GetHeight())/2 ;

        offset_w = (offset_w<0)? 0 : offset_w ;
        offset_h = (offset_h<0)? 0 : offset_h ;

        dc.DrawBitmap(m_bitmap, offset_w, offset_h, false);
    }

    void OnSize(wxSizeEvent& event)
    {
        SetScrollbars(1,1, m_w, m_h, 0, 0);
        Refresh();
        event.Skip();
    }

    void checkZoom( )
    {
        int ww = m_image.GetWidth();
        int hh = m_image.GetHeight();

        if ( m_zoom < 0.0 )
            m_zoom = 1.0 ;
        if ( m_zoom ==  0.0 )
            m_zoom = (float) m_w / (float) m_image.GetWidth();

        wxSize screenSize = wxGetDisplaySize();
        screenSize.x *= 3 ;
        if ( (m_zoom*ww) >screenSize.x )
        {
            m_zoom = (float) screenSize.x / (float) m_image.GetWidth();
        }
        screenSize.y *= 3 ;
        if ( (m_zoom*hh) >screenSize.y )
        {
            m_zoom = (float) screenSize.y / (float) m_image.GetHeight();
        }

        if ( (m_zoom*ww) < 4.0 )
        {
            m_zoom = 4.0f/float(ww);
        }
        if ( (m_zoom*hh) < 4.0 )
        {
            m_zoom = 4.0f/float(hh);
        }
    }
    void setZoom( float value )
    {
        if ( value == 0 )
        {
            int w_DC, h_DC;
            wxClientDC dc(this);
            dc.GetSize( &w_DC, &h_DC );

            FitImage(w_DC,h_DC) ;
            m_zoom = 0 ;
            SetScrollbars(1,1, m_w, m_h, 0, 0);
        }
        else if ( value == 1.0 )
        {
            m_w = m_image.GetWidth();
            m_h = m_image.GetHeight();
            m_zoom = 1.0 ;
            SetScrollbars(1,1, m_w, m_h, 0, 0);
        }
        else if ( value == 2.0f )
        {
            m_zoom *= 1.5f ;
            checkZoom() ;

            m_w = m_zoom * m_image.GetWidth();
            m_h = m_zoom * m_image.GetHeight();
            SetScrollbars(1,1, m_w, m_h, 0, 0);
        }
        else if ( value == -2.0f )
        {
            m_zoom /= 1.5f ;
            checkZoom() ;
            m_w = m_zoom * m_image.GetWidth();
            m_h = m_zoom * m_image.GetHeight();
            SetScrollbars(1,1, m_w,m_h, 0, 0);
        }

        Refresh();
    }

    void FlipImage( bool horizontal = true )
    {
        m_image = m_image.Mirror( horizontal ) ;
        Refresh();
    }
    void RotImage( int valeur )
    {
        switch ( valeur )
        {
            case 1 :
                m_image = m_image.Rotate90( true ) ;
                break ;
            case 2 :
                m_image = m_image.Rotate180(  ) ;
                break ;
            case 3 :
                m_image = m_image.Rotate90( false ) ;
                break ;
            default:
                break ;
        }

        m_w = m_image.GetWidth();
        m_h = m_image.GetHeight();
        m_ratio_w_h = (float) m_image.GetWidth() / (float) m_image.GetHeight() ;

        if ( m_zoom !=  0 )
            setZoom(m_zoom);

        Refresh();
    }

    void setWhiteBalance( float kR, float kV, float kB )
    {
        //uint8_t *data = m_image.GetData() ;
        //float value ;
        int w = m_image.GetWidth() ;
        int h = m_image.GetHeight() ;
        for ( uint16_t xx=0; xx<w;xx++)
        {
            for ( uint16_t yy=0; yy<h;yy++)
            {
                float red   = ((float) m_image.GetRed( xx,yy   )) * kR;
                float green = ((float) m_image.GetGreen( xx,yy )) * kV;
                float blue  = ((float) m_image.GetBlue( xx,yy  )) * kB;

                if ( red > 255.0f   ) red   = 255.0 ;
                if ( green > 255.0f ) green = 255.0 ;
                if ( blue > 255.0f  ) red   = 255.0 ;

                m_image.SetRGB( xx, yy, (uint8_t) (red+0.5f),(uint8_t) (green+0.5f),(uint8_t) (blue+0.5f) );
            }
        }
        Refresh();
    }

    void ChangeSeuil( float bas, float haut )
    {
        if ( bas >= haut)
            return ;
        uint16_t hh  = m_imgfit->getLigne() ;
        uint16_t ww  = m_imgfit->getColonne() ;

        if ( (hh > 0) && (ww > 0))
        {
            uint8_t *rgbArray = m_imgfit->ArrayRGB8(bas,haut) ;

            m_image.SetData(rgbArray,ww,hh,false);
        }
        Refresh();
    }

    uint32_t * ArrayHistogram( uint8_t nplan )
    {
        if (nplan >  3)
            return nullptr ;

        uint32_t  nn = 3 ;
        uint32_t  bin = 256 ;
        uint32_t  nItems =  m_image.GetWidth() * m_image.GetHeight();
        uint8_t   *m_data=  m_image.GetData() ;

        uint32_t *bHistogram = new uint32_t [bin+nn] ;

        for ( uint32_t ii=0;ii<bin+nn;ii++)
            bHistogram[ii] = 0 ;

        uint32_t  offset = (nplan==0)? 0 : nplan-1  ;
        uint32_t  step   = (nplan==0)? 1 : 3 ;
        nItems *=  (nplan==0)? 3 : 1 ;

        for (uint32_t ii = 0; ii < nItems ; ii++,offset+=step)
        {
            bHistogram[m_data[offset]]++ ;
        }


        for ( uint32_t ii = 1; ii < bin ; ii++)
        {
            if ( bHistogram[ii] != 0 )
            {
                bHistogram[bin] = ii ;
                break ;
            }
        }
        for ( uint32_t ii = 1; ii < bin ; ii++)
        {
            if ( bHistogram[bin-1-ii] != 0 )
            {
                bHistogram[bin+1] = bin-1-ii ;
                break ;
            }
        }

        uint32_t n_pts=0 ;
        for ( uint32_t ii = 1; ii < bin-1 ; ii++)
        {
            n_pts+=bHistogram[ii] ;
        }

        uint32_t sum=0 ;
        for ( uint32_t ii = 1; ii < bin-1 ; ii++)
        {
            sum+=bHistogram[ii] ;
            if ( sum>= n_pts/2 )
            {
                bHistogram[bin+2] = ii ;
                break ;
            }
        }

        return bHistogram ;
    }
};

#endif /* WXSCROLLIMAGE_HPP */
