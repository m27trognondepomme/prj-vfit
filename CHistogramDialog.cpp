#include <wx/wx.h>
#include <wx/sizer.h>
#include <wx/string.h>
#include <wx/intl.h>
#include "CHistogramDialog.h"

BEGIN_EVENT_TABLE(CHistogramDialog,wxDialog)
END_EVENT_TABLE()

CHistogramDialog::CHistogramDialog(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size)
{
	Create(parent, id, _("Histogram"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE, _T("id"));
	SetClientSize(wxSize(530,530));
	Move(wxDefaultPosition);

	wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);

	m_LHistoPanel = new CHistoPanel((wxWindow*)this) ;
	sizer->Add(m_LHistoPanel, 1, wxEXPAND);

	m_RHistoPanel = new CHistoPanel((wxWindow*)this) ;
	sizer->Add(m_RHistoPanel, 1, wxEXPAND);

	m_VHistoPanel = new CHistoPanel((wxWindow*)this) ;
	sizer->Add(m_VHistoPanel, 1, wxEXPAND);

	m_BHistoPanel = new CHistoPanel((wxWindow*)this) ;
	sizer->Add(m_BHistoPanel, 1, wxEXPAND);

	SetSizer(sizer);

	m_l_bmp = nullptr ;
	m_r_bmp = nullptr ;
	m_v_bmp = nullptr ;
	m_b_bmp = nullptr ;
	m_bLog = false  ;
}

CHistogramDialog::~CHistogramDialog()
{
    Destroy();
}

wxBitmap *CHistogramDialog::CreateHistoBitmap(int nplan, uint32_t bin ,uint32_t *hist)
{
    if ( hist == nullptr )
        return nullptr ;

    uint32_t ii ;
    wxMemoryDC memDC;
    uint32_t w = 512 ;
    uint32_t h = w/4 ;

    wxBitmap *bitmap = new wxBitmap(w,h);
    memDC.SelectObject(*bitmap);
    memDC.SetBackground(*wxWHITE_BRUSH);
    memDC.Clear();

    memDC.SetBrush(*wxTRANSPARENT_BRUSH);
    memDC.DrawRectangle(wxRect(0, 0, w, h));

    if (nplan==0)
    {
        memDC.DrawText(wxT("L"), w-20, 20 );
        memDC.SetPen(*wxGREY_PEN);
        memDC.SetBrush(*wxGREY_BRUSH);
    }

    if (nplan==1)
    {
        memDC.DrawText(wxT("R"), w-20, 20 );
        memDC.SetPen(*wxRED_PEN);
        memDC.SetBrush(*wxRED_BRUSH);
    }

    if (nplan==2)
    {
        memDC.DrawText(wxT("V"), w-20, 20 );
        memDC.SetPen(*wxGREEN_PEN);
        memDC.SetBrush(*wxGREEN_BRUSH);
    }

    if (nplan==3)
    {
        memDC.DrawText(wxT("B"), w-20, 20 );
        memDC.SetPen(*wxBLUE_PEN);
        memDC.SetBrush(*wxBLUE_BRUSH);
    }

    uint32_t vmax = hist[0] ;
    for ( ii=1; ii < bin; ii++ )
        vmax = vmax > hist[ii] ? vmax : hist[ii] ;

    wxPoint *polygon = new wxPoint[bin+2] ;
    int step = w/bin ;
    float inv_vmax ;

    if ( m_bLog )
        inv_vmax = 1.f / log10(vmax) ;
    else
        inv_vmax = 1.f / (float) vmax ;

    polygon[0] = wxPoint(0,h) ;
    for ( ii=0; ii < bin; ii++ )
    {
        int yvalue ;

        if ( m_bLog )
        {
            float fvalue = hist[ii]!=0 ? log10( (float) hist[ii] ) : log10(0.01) ;
            yvalue = (int) ( (float) h * (1.1f-fvalue*inv_vmax)) ;
        }
        else
            yvalue = (int) ( (float) h * (1.0f- (float) hist[ii] * inv_vmax )) ;
        polygon[ii+1] = wxPoint(ii*step,(int) yvalue) ;
    }
    polygon[ii+1] = wxPoint((ii-1)*step ,h) ;

    memDC.DrawPolygon(bin+2,polygon);
    memDC.SetPen(*wxBLACK_PEN);

    // graduations de l'abscisse
    memDC.DrawLine(wxPoint(1*w/8, h-0), wxPoint(1*w/8, h-6 ) ) ;
    memDC.DrawLine(wxPoint(  w/4, h-0), wxPoint(  w/4, h-8) ) ;
    memDC.DrawLine(wxPoint(3*w/8, h-0), wxPoint(3*w/8, h-6 ) ) ;
    memDC.DrawLine(wxPoint(  w/2, h-0), wxPoint(  w/2, h-10) ) ;
    memDC.DrawLine(wxPoint(5*w/8, h-0), wxPoint(5*w/8, h-6 ) ) ;
    memDC.DrawLine(wxPoint(3*w/4, h-0), wxPoint(3*w/4, h-8) ) ;
    memDC.DrawLine(wxPoint(7*w/8, h-0), wxPoint(7*w/8, h-6 ) ) ;

    // Min/Max
    //memDC.DrawLine(wxPoint(hist[bin+0]*step, h), wxPoint(hist[bin+0]*step, 0 ) ) ;
    //memDC.DrawLine(wxPoint(hist[bin+1]*step, h), wxPoint(hist[bin+1]*step, 0 ) ) ;

    // Moyenne
    memDC.SetPen(*wxBLACK_DASHED_PEN );
    memDC.DrawLine(wxPoint(hist[bin+2]*step, h), wxPoint(hist[bin+2]*step, 0 ) ) ;

    memDC.SelectObject(wxNullBitmap);
    return bitmap;
}

void CHistogramDialog::SetHistogram( uint32_t bin ,uint32_t *l_hist, uint32_t *r_hist, uint32_t *v_hist, uint32_t *b_hist)
{
    m_l_bmp = CreateHistoBitmap(0,bin,l_hist);
    m_r_bmp = CreateHistoBitmap(1,bin,r_hist);
    m_v_bmp = CreateHistoBitmap(2,bin,v_hist);
    m_b_bmp = CreateHistoBitmap(3,bin,b_hist);

    if ( (m_r_bmp == nullptr ) && (m_v_bmp == nullptr ) && (m_b_bmp == nullptr ) )
        SetClientSize(wxSize(530,140));
    else
        SetClientSize(wxSize(530,530));

    m_LHistoPanel->setBitmap( m_l_bmp ) ;
    m_RHistoPanel->setBitmap( m_r_bmp ) ;
    m_VHistoPanel->setBitmap( m_v_bmp ) ;
    m_BHistoPanel->setBitmap( m_b_bmp ) ;

    Refresh();
    Update();
}
