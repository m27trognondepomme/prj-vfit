��    b      ,  �   <      H     I     O     \     h     t     �     �     �     �     �     �     �     �  &   �     	     '	  !   E	     g	     �	     �	  
   �	     �	  
   �	     �	     �	  	   �	     �	     �	     �	     

     
     *
     :
     I
     U
  
   b
  	   m
     w
     �
     �
     �
     �
     �
     �
     �
            
   (     3     L     Z  #   k     �     �  
   �     �     �     �     �     �     �     �               "     1     6     K     f     �     �     �     �  
   �  	   �     �     �      �  	             (  T   -  �   �  	        #     (     6     ;     B  	   I     S     [  
   d  	   o  	   y  $   �     �  <  �     �     �       
        !     0     7     K     R     `     i     r     �  +   �  /   �  '   �  )     1   D     v     �     �     �     �     �     �     �     �                6     L     `  
   v     �     �     �     �  #   �     �     �  #     $   &  #   K     o     v     �     �     �     �  	   �     �  &   �               &     5     A     D     K     `     z     �     �     �     �     �     �                ;     D     V     h     x     �     �     �  6   �     �     �       j     �   �     3     9     >     Q     V     ]  	   d     n     v          �     �  +   �     �     \   ?       L            C   *   b           U   ^       I   %         ;   8          ]   @       2   
           -   (   J   ,   N   6             X          !   Z   M      R   :   A               >       S       a   `                 7   <   F          9   W          4   &   .   P   K   G      #               [   5                     O   +         D   "   H   B                 T   /           0      Y              E   '      _           V   Q   3       =       )   	           1   $    &File &First image &Last image &Navigation &Next image &Open &Previous image  &Quit	Alt-F4 &Visualization About About	F1 Auto Threshold Auto threshold Automatic tuning of display thresholds Cannot open file '%s'. (Exist?) Cannot open file '%s'. (fit?) Display the Histogram of FIT file Display the Histogram of screen Display threshold End FIT Header FIT Header  FIT header First image Fit FitHeader Flip  Left/Right Flip Left/Right Flip Top/Bottom Flip Top/bottom Flip left/right Flip top/bottom High Threshold Histo Image Histo screen HistoImage Histogram Histogram Linear/Log Histogram Pref Histogram Screen Histogram linear/Logarithmic Histogram of FIT file Histogram of the screen Home Image Histogram Image Statistic Information Last image Left rotate of the image Low threshold Manual threshold Manual tuning of display thresholds Menu Next Next image NoZoom OK Open Open a FIT image Open the first image Open the last image Open the next image Open the previous image Previous Previous image Quit Quit the application Right  rotate of the image Right rotate of the image Rotate Rotate +90 deg. Rotate -90 deg. Rotate Right RotateLeft Scale 1:1 Scale : 1/1 Screen Histogram Show info about this application Statistic Statistic of FIT image Text This program is a viewer of FIT image
(Grayscale and RGB  [8,16,32bits] format only) This program is provided without any guarantee.
For details, see GNU General Public License, version 3 or later.
https://www.gnu.org/licenses/gpl.html Threshold VFIT White balance Zoom Zoom + Zoom - Zoom -out Zoom in Zoom out auto scale auto zoom scale 1/1 switch linear or logarithmic display zoom fit Project-Id-Version: VFIT
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-12-26 23:52+0100
PO-Revision-Date: 2017-12-26 23:52+0100
Last-Translator: M27trognondepomme <pebe92@gmail.com>
Language-Team: VFIT French
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Fichier Première image Dernière image Navigation Image suivante Ouvrir Image précédente  Sortir Visualisation A propos A propos Seuil automatique Seuil automatique Réglage automatique des seuils d'affichage Erreur ouverture du fichier '%s' (Existe-t-il?) Erreur ouverture du fichier '%s' (FIT?) Affichage de l'histogramme du fichier FIT Affichage de l'histogramme de l'image à l'écran Seuils de visualisation Fin Entête de l'image FIT Entête de l'image FIT  Entête de l'image FIT Première image FIT Entête FIT Symétrie verticale Symétrie verticale Symétrie horizontale Symétrie horizontale Symétrie verticale Symétrie horizontale Seuil haut Histo Image Histo écran Histo Image Histogramme Histogramme linéaire/Logarithmique Préférence histogramme Histogramme écran Histogramme linéaire/Logarithmique Histogramme de l'image FIT (fichier) Histogramme de l'image FIT (écran) Début Histogramme de l'image Statistique de l'image Information Dernière image Rotation gauche de l'image Seuil bas Seuil manuel Réglage manuel des seuils d'affichage Menu Suivant Image Suivante Pas de zoom OK Ouvrir Ouvrir une image FIT Ouvrir la première image Ouvrir la dernière image Ouvrir l'image suivante Ouvrir l'image précédente Précédente Image précédente Sortir Quitter l'application Rotation gauche de l'image Rotation gauche de l'image Rotation Rotation +90 deg. Rotation -90 deg. Rotation droite Rotation gauche Échelle 1:1 Échelle 1:1 Histogramme de l'écran Afficher une information a propos de cette application Statistique Statistique de l'image Texte Ce programme est un visualisateur d'image FIT
( uniquement les formats : monochrome et RVB [8,16,32bits] ) Ce programme est fourni sans aucune garantie.
Pour plus de détails, visitez Licence publique générale GNU, version 3 ou ultérieure.
https://www.gnu.org/licenses/gpl.html Seuil VFIT Equilibre du blanc Zoom Zoom + Zoom - Zoom -out Zoom in Zoom out échelle automatique échelle automatique échelle 1:1 commute l'affichage linéaire/logarithmique zoom fit 