#include <wx/wx.h>
#include "CHistoPanel.h"

BEGIN_EVENT_TABLE(CHistoPanel, wxPanel)
// some useful events
/*
 EVT_MOTION(CHistoPanel::mouseMoved)
 EVT_LEFT_DOWN(CHistoPanel::mouseDown)
 EVT_LEFT_UP(CHistoPanel::mouseReleased)
 EVT_RIGHT_DOWN(CHistoPanel::rightClick)
 EVT_LEAVE_WINDOW(CHistoPanel::mouseLeftWindow)
 EVT_KEY_DOWN(CHistoPanel::keyPressed)
 EVT_KEY_UP(CHistoPanel::keyReleased)
 EVT_MOUSEWHEEL(CHistoPanel::mouseWheelMoved)
 */

// catch paint events
EVT_PAINT(CHistoPanel::paintEvent)

END_EVENT_TABLE()


// some useful events
/*
 void CHistoPanel::mouseMoved(wxMouseEvent& event) {}
 void CHistoPanel::mouseDown(wxMouseEvent& event) {}
 void CHistoPanel::mouseWheelMoved(wxMouseEvent& event) {}
 void CHistoPanel::mouseReleased(wxMouseEvent& event) {}
 void CHistoPanel::rightClick(wxMouseEvent& event) {}
 void CHistoPanel::mouseLeftWindow(wxMouseEvent& event) {}
 void CHistoPanel::keyPressed(wxKeyEvent& event) {}
 void CHistoPanel::keyReleased(wxKeyEvent& event) {}
 */

CHistoPanel::CHistoPanel(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size) :
wxPanel(parent,id,pos,size)
{
    m_bitmap = nullptr ;
    SetClientSize(wxSize(530,140));
}

void CHistoPanel::paintEvent(wxPaintEvent & evt)
{
    wxPaintDC dc(this);
    render(dc);
}

void CHistoPanel::paintNow()
{
    wxClientDC dc(this);
    render(dc);
}

void CHistoPanel::render(wxDC&  dc)
{
    if ( m_bitmap == nullptr )
        return ;

    int w_DC, h_DC;
    dc.GetSize( &w_DC, &h_DC );

    int offset_w = (w_DC - m_bitmap->GetWidth())/2 ;
    int offset_h = (h_DC - m_bitmap->GetHeight())/2 ;

    offset_w = (offset_w<0)? 0 : offset_w ;
    offset_h = (offset_h<0)? 0 : offset_h ;

    dc.DrawBitmap(*m_bitmap, offset_w, offset_h, false);
}

void CHistoPanel::setBitmap( wxBitmap *bmp )
{
    m_bitmap = bmp ;
    if ( m_bitmap == nullptr )
        Hide() ;

    Refresh();
    Update();
}
