#ifndef CHISTOGRAMDIALOG_H
#define CHISTOGRAMDIALOG_H

#include <wx/dialog.h>
#include "CHistoPanel.h"

class CHistogramDialog: public wxDialog
{
	public:

		CHistogramDialog(wxWindow* parent,wxWindowID id=wxID_ANY,const wxPoint& pos=wxDefaultPosition,const wxSize& size=wxDefaultSize);
		virtual ~CHistogramDialog();

		void SetTitle( wxString title ) { wxTopLevelWindow::SetTitle(title) ; }

		void SetHistogram( uint32_t bin ,uint32_t *l_hist, uint32_t *r_hist, uint32_t *v_hist, uint32_t *b_hist);

		void SetMode( bool valeur ) { m_bLog =  valeur ;}
		void ToggleMode(  ) { m_bLog =  m_bLog ? false: true ;}

	protected:

        CHistoPanel *m_LHistoPanel ;
        CHistoPanel *m_RHistoPanel ;
        CHistoPanel *m_VHistoPanel ;
        CHistoPanel *m_BHistoPanel ;
	    void deleteHistoBitmap() ;
        wxBitmap *CreateHistoBitmap(int nplan, uint32_t bin ,uint32_t *hist) ;

	    wxBitmap *m_l_bmp ;
	    wxBitmap *m_r_bmp ;
	    wxBitmap *m_v_bmp ;
	    wxBitmap *m_b_bmp ;
	    bool m_bLog ;

	private:

		DECLARE_EVENT_TABLE()
};

#endif
