/***************************************************************
 * Name:      vfitMain.h
 * Purpose:   Defines Application Frame
 * Author:    M27trognondepomme ()
 * Created:   2016-11-05
 * Copyright: M27trognondepomme ()
 * License:
 **************************************************************/

#ifndef VFITMAIN_H
#define VFITMAIN_H

#include "WxScrollImage.hpp"
#include "CSeuilDialog.h"
#include "CInfoDialog.h"
#include "CHistogramDialog.h"

//(*Headers(vfitFrame)
#include <wx/toolbar.h>
#include <wx/menu.h>
#include <wx/frame.h>
//*)

class vfitFrame: public wxFrame
{
    public:

        vfitFrame(wxWindow* parent, wxWindowID id = -1 );
        virtual ~vfitFrame();

        bool LoadImage( wxString filename ) ;

    private:
        wxBitmap           vfit_bitmap   ;
        wxWindow           *m_parent     ;
        wxMenuBar          *m_menubar    ;
        wxString            m_fitname    ;
        ScrolledImagePanel *m_ImagePanel ;
        CSeuilDialog       *m_SeuilDlg   ;
        CInfoDialog        *m_InfoDlg    ;
        CHistogramDialog   *m_HistoDlg   ;
        wxString            m_info       ;

        uint16_t            m_nTotalFit  ;
        uint16_t            m_nCurFit    ;
        int8_t              m_toggleMenu ;
        bool                m_HistoEcran ;
        int8_t              m_toggleWB   ;

        bool OpenImage( wxString filename ) ;
        void InitSeuil() ;
        void FormatDisplayStatistic( wxString label, bool rgb, float *value , float range);

        void GetIndexFit() ;
        void LoadIndexFit(uint16_t no_fit) ;

        //(*Handlers(vfitFrame)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        void OnFitImage(wxCommandEvent& event);
        void OnEchelle1(wxCommandEvent& event);
        void OnZoomPlus(wxCommandEvent& event);
        void OnZoomMoins(wxCommandEvent& event);
        void OnFlipV(wxCommandEvent& event);
        void OnFlipH(wxCommandEvent& event);
        void OnRot90(wxCommandEvent& event);
        void OnRot270(wxCommandEvent& event);
        void OnSeuil(wxCommandEvent& event);
        void OnStatistic(wxCommandEvent& event);
        void OnHistogram(wxCommandEvent& event);
        void OnHistogramVisu(wxCommandEvent& event);
        void OnLineaireLog(wxCommandEvent& event);
        void OnHeaderInfo(wxCommandEvent& event);
        void OnSeuilAuto(wxCommandEvent& event);
        void OnOpenFit(wxCommandEvent& event);
        void OnNextImage(wxCommandEvent& event);
        void OnPreviousImage(wxCommandEvent& event);
        void OnFirstImage(wxCommandEvent& event);
        void OnLastImage(wxCommandEvent& event);
        void OnVisibleMenu(wxCommandEvent& event);
        void OnWhiteBalance(wxCommandEvent& event);
        //*)

        //(*Identifiers(vfitFrame)
        static const long ID_OPEN_FIT;
        static const long idMenuQuit;
        static const long ID_NEXT;
        static const long ID_PREC;
        static const long ID_FIRST;
        static const long ID_LAST;
        static const long ID_MENUITEM8;
        static const long ID_MENUITEM15;
        static const long ID_WB;
        static const long ID_MENUITEM10;
        static const long ID_MENUITEM11;
        static const long ID_MENUITEM12;
        static const long ID_MENUITEM13;
        static const long ID_MENUITEM7;
        static const long ID_MENUITEM9;
        static const long ID_MENUITEM5;
        static const long ID_MENUITEM6;
        static const long ID_MENUITEM1;
        static const long ID_MENUITEM2;
        static const long ID_MENUITEM3;
        static const long ID_MENUITEM4;
        static const long ID_MENUITEM14;
        static const long idMenuAbout;
        static const long IDB_QUIT;
        static const long IDB_OPEN;
        static const long IDB_HOME;
        static const long IDB_PREVIOUS;
        static const long IDB_NEXT;
        static const long IDB_END;
        static const long IDB_PZOOM;
        static const long IDB_MZOOM;
        static const long IDB_FIT;
        static const long IDB_NOZOOM;
        static const long ID_TOOLBARITEM4;
        static const long IDB_SEUIL;
        static const long IDB_WB;
        static const long IDB_HISTOIMG;
        static const long IDB_HISTOECRAN;
        static const long IDB_HISTOPREF;
        static const long ID_TOOLBARITEM1;
        static const long IDB_STAT_IMAGE;
        static const long IDB_ROTATELEFT;
        static const long IDB_ROTATERIGHT;
        static const long IDB_FLIPH;
        static const long IDB_FLIPV;
        static const long IDB_MENU;
        static const long ID_TOOLBARITEM3;
        static const long ID_TOOLBAR1;
        //*)

        //(*Declarations(vfitFrame)
        wxToolBarToolBase* ToolBarItem14;
        wxToolBarToolBase* ToolBarItem16;
        wxMenuItem* MenuItem23;
        wxToolBarToolBase* ToolBarItem24;
        wxToolBarToolBase* ToolBarItem5;
        wxToolBarToolBase* ToolBarItem21;
        wxMenu* Menu5;
        wxMenuItem* MenuItem16;
        wxToolBarToolBase* ToolBarItem11;
        wxMenuItem* MenuItem12;
        wxToolBarToolBase* ToolBarItem18;
        wxToolBarToolBase* ToolBarItem6;
        wxMenu* Menu3;
        wxMenuItem* MenuItem19;
        wxMenuItem* MenuItem20;
        wxToolBarToolBase* ToolBarItem17;
        wxToolBarToolBase* ToolBarItem12;
        wxMenuItem* MenuItem15;
        wxToolBarToolBase* ToolBarItem20;
        wxToolBarToolBase* ToolBarItem7;
        wxToolBarToolBase* ToolBarItem2;
        wxMenuItem* MenuItem17;
        wxMenuItem* MenuItem21;
        wxToolBarToolBase* ToolBarItem13;
        wxToolBarToolBase* ToolBarItem9;
        wxToolBarToolBase* ToolBarItem23;
        wxMenuItem* MenuItem3;
        wxMenuItem* MenuItem9;
        wxMenu* Menu7;
        wxToolBarToolBase* ToolBarItem10;
        wxMenu* Menu4;
        wxToolBar* ToolBar1;
        wxMenuItem* MenuItem11;
        wxMenuItem* MenuItem22;
        wxToolBarToolBase* ToolBarItem4;
        wxMenuItem* MenuItem5;
        wxToolBarToolBase* ToolBarItem1;
        wxMenuItem* MenuItem10;
        wxMenuItem* MenuItem18;
        wxToolBarToolBase* ToolBarItem3;
        wxMenuItem* MenuItem6;
        wxMenuItem* MenuItem4;
        wxMenuItem* MenuItem7;
        wxMenuItem* MenuItem13;
        wxToolBarToolBase* ToolBarItem19;
        wxToolBarToolBase* ToolBarItem15;
        wxToolBarToolBase* ToolBarItem8;
        wxToolBarToolBase* ToolBarItem22;
        wxMenuItem* MenuItem8;
        wxMenuItem* MenuItem14;
        //*)

        DECLARE_EVENT_TABLE()
};

#endif // VFITMAIN_H
