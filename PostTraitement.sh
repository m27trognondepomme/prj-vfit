#!/bin/bash
echo "Postprocessing ..."
TARGET_OUTPUT_DIR=./share/vfit/
mkdir -p ${TARGET_OUTPUT_DIR}
rm -f ${TARGET_OUTPUT_DIR}toolbar/*.png
cp -r  ./img/toolbar ${TARGET_OUTPUT_DIR}
cp -r ./po/locale ${TARGET_OUTPUT_DIR}
