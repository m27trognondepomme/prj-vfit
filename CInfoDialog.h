#ifndef CINFODIALOG_H
#define CINFODIALOG_H

//(*Headers(CInfoDialog)
#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/button.h>
#include <wx/textctrl.h>
//*)

class CInfoDialog: public wxDialog
{
	public:

		CInfoDialog(wxWindow* parent,wxWindowID id=wxID_ANY,const wxPoint& pos=wxDefaultPosition,const wxSize& size=wxDefaultSize);
		virtual ~CInfoDialog();

		void SetText( wxString value ) ;
		//(*Declarations(CInfoDialog)
		wxButton* Button1;
		wxTextCtrl* TextCtrl1;
		//*)

	protected:

		//(*Identifiers(CInfoDialog)
		static const long ID_TEXTCTRL1;
		static const long ID_BUTTON1;
		//*)

	private:

		//(*Handlers(CInfoDialog)
		void OnButton1Click(wxCommandEvent& event);
		//*)

		DECLARE_EVENT_TABLE()
};

#endif
