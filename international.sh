#!/bin/bash
# example sh script to generate catalog file
# it searches for strings marked with _() in all .cpp files in directory ./src
# the generated file is called 'app_name.pot' and is generated in ./po
CPP_FILE_LIST=`find ./ -name '*.cpp' -print`
HPP_FILE_LIST=`find ./ -name '*.hpp' -print`
H_FILE_LIST=`find ./ -name '*.h' -print`
xgettext --package-name VFIT --no-location -j -d vfit -s --keyword=_ -p ./po -o vfit.pot $CPP_FILE_LIST $HPP_FILE_LIST $H_FILE_LIST 
DATE=`date '+%Y-%m-%d %H:%M%z'`
sed -e "s/YEAR-MO-DA HO:MI+ZONE/$DATE/" -e 's/^"Language: /"Language: fr_FR/' \
   -e 's/FULL NAME <EMAIL@ADDRESS>/M27trognondepomme <pebe92@gmail.com>/' \
   -e 's/LANGUAGE <LL@li.org>/VFIT French/' ./po/vfit.pot >  ./po/locale/fr_FR/LC_MESSAGES/vfit.po
   
msgfmt  --output-file=./po/locale/fr_FR/LC_MESSAGES/vfit.mo ./po/locale/fr_FR/LC_MESSAGES/vfit.po 
